using System;
using System.Diagnostics;

namespace OdinInspector
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    [Conditional("UNITY_EDITOR")]
    public class DarkBoxAttribute : Attribute
    {
        public readonly bool WithBorders;

        public DarkBoxAttribute()
        {
            WithBorders = true;
        }
        
        public DarkBoxAttribute(bool withBorders = true)
        {
            WithBorders = withBorders;
        }
    }
}