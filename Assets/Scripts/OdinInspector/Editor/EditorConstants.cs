using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace OdinInspector.Editor
{
    public static class EditorConstants
    {
        public static readonly List<ValueDropdownItem> BoolDropdownItems = new()
        {
            new ValueDropdownItem("YES", true),
            new ValueDropdownItem("NO", false)
        };
    }
}