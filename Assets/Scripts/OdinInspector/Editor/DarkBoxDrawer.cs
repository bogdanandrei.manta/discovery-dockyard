using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace OdinInspector.Editor
{
    [DrawerPriority(0, 99)]
    public class DarkBoxDrawer : OdinAttributeDrawer<DarkBoxAttribute>
    {
        private readonly Color _borderColor = EditorGUIUtility.isProSkin
            ? Color.Lerp(Color.black, Color.white, 0.1f)
            : Color.gray;

        private readonly Color _boxColor = new(0, 0, 0, 0.15f);

        protected override void DrawPropertyLayout(GUIContent label)
        {
            BoxGUI.BeginBox(_boxColor);
            CallNextDrawer(label);

            BoxGUI.EndBox(Attribute.WithBorders ? _borderColor : null);
        }
    }

    internal static class BoxGUI
    {
        private static Rect _currentLayoutRect;

        public static void BeginBox(Color color)
        {
            _currentLayoutRect = EditorGUILayout.BeginVertical(SirenixGUIStyles.None);

            if (Event.current.type == EventType.Repaint)
            {
                SirenixEditorGUI.DrawSolidRect(_currentLayoutRect, color);
            }
        }

        public static void EndBox(Color? borders = null)
        {
            EditorGUILayout.EndVertical();

            if (Event.current.type == EventType.Repaint && borders != null)
            {
                SirenixEditorGUI.DrawBorders(_currentLayoutRect, 1, 1, 1, 1, borders.Value);
            }

            GUILayout.Space(5);
        }
    }
}