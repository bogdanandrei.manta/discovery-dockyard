#if DOTWEEN
using DG.Tweening;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace DoTween
{
    public class GraphicFadeDoTween : BaseDoTween
    {
        [Title("Fade Settings")]
        [DarkBox]
        [PropertyOrder(10)]
        [SerializeField]
        private float _value;

        [HideInInspector]
        [SerializeField]
        private Graphic _graphic;
        
        protected override void OnValidate()
        {
            if (_targetTransform == null)
            {
                return;
            }
            
            _graphic = _targetTransform.GetComponent<Graphic>();
        }

        protected override bool ValidateTarget(Transform target, out string errorMsg)
        {
            if (target == null)
            {
                errorMsg = "Target cannot be null!";
                return false;
            }

            var graphic = target.GetComponent<Graphic>();

            if (graphic == null)
            {
                errorMsg = "Target must have a Graphic component!";
                return false;
            }

            errorMsg = string.Empty;
            return true;
        }

        public override Tweener CreateTween()
        {
            return _graphic.DOFade(_value, _duration).SetAs(GetTweenParams());
        }
    }
}
#endif