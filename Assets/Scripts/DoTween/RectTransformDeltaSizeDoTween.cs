using DG.Tweening;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace DoTween
{
    public class RectTransformDeltaSizeDoTween : BaseDoTween
    {
        [Title("Size Settings")]
        [PropertyOrder(10)]
        [DarkBox]
        [SerializeField]
        private Vector2 _targetSizeDelta;

        [HideInInspector]
        [SerializeField]
        private RectTransform _rectTransform;

        protected override void OnValidate()
        {
            if (_targetTransform == null)
            {
                return;
            }
            
            _rectTransform = _targetTransform.GetComponent<RectTransform>();
        }
        
        protected override bool ValidateTarget(Transform target, out string errorMsg)
        {
            if (target == null)
            {
                errorMsg = "Target Transform is null!";
                return false;
            }

            if (target.GetComponent<RectTransform>() == null)
            {
                errorMsg = "Target Transform is not a RectTransform!";
                return false;
            }

            errorMsg = string.Empty;
            return true;
        }
        
        public override Tweener CreateTween()
        {
            return _rectTransform.DOSizeDelta(_targetSizeDelta, _duration).SetAs(GetTweenParams());
        }
    }
}