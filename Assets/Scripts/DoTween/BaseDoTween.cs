#if DOTWEEN
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using JetBrains.Annotations;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace DoTween
{
    public abstract class BaseDoTween : MonoBehaviour
    {
        [Title("Target")]
        [ValidateInput(nameof(ValidateTarget), nameof(TargetValidationError))]
        [HideLabel]
        [DarkBox]
        [SerializeField]
        protected Transform _targetTransform;

        [TitleGroup("When to Auto Run")]
        [HorizontalGroup("When to Auto Run/Split", Width = .33f)]
        [ValueDropdown("@OdinInspector.Editor.EditorConstants.BoolDropdownItems")]
        [LabelText("On Awake")]
        [OnValueChanged(nameof(RunOnAwakeChanged))]
        [DarkBox]
        [SerializeField]
        private bool _runOnAwake;

        [HorizontalGroup("When to Auto Run/Split", Width = .33f)]
        [ValueDropdown("@OdinInspector.Editor.EditorConstants.BoolDropdownItems")]
        [OnValueChanged(nameof(RunOnStartChanged))]
        [LabelText("On Start")]
        [DarkBox]
        [SerializeField]
        private bool _runOnStart;

        [HorizontalGroup("When to Auto Run/Split", Width = .33f)]
        [ValueDropdown("@OdinInspector.Editor.EditorConstants.BoolDropdownItems")]
        [OnValueChanged(nameof(RunOnEnableChanged))]
        [LabelText("On Enable")]
        [DarkBox]
        [SerializeField]
        private bool _runOnEnable;

        [Title("How to Auto Run")]
        [HideLabel]
        [ValueDropdown(nameof(_autoFromDropdownItems))]
        [DarkBox]
        [SerializeField]
        private bool _autoFrom;

        [TitleGroup("Looping")]
        [HorizontalGroup("Looping/Split", Width = .33f)]
        [DarkBox]
        [Tooltip("Set to -1 for infinite loops.")]
        [MinValue(-1)]
        [SerializeField]
        private int _loops;

        [HorizontalGroup("Looping/Split", Width = .63f)]
        [ShowIf("@_loops != 0")]
        [DarkBox]
        [SerializeField]
        private LoopType _loopType;

        [Title("Animation Settings")]
        [DarkBox]
        [SerializeField]
        protected float _duration;
        
        [DarkBox]
        [SerializeField]
        protected float _delay;

        [DarkBox]
        [SerializeField]
        private Ease _easeType;

        [Title("Events")]
        [PropertyOrder(100)]
        [SerializeField]
        public UnityEvent _onStartEvent = new();

        [PropertyOrder(100)]
        [SerializeField]
        public UnityEvent _onCompleteEvent = new();

        [UsedImplicitly]
        protected string TargetValidationError;

        private readonly List<ValueDropdownItem<bool>> _autoFromDropdownItems = new()
        {
            new ValueDropdownItem<bool>("RUN FROM VALUE", true),
            new ValueDropdownItem<bool>("RUN TO VALUE", false)
        };

        protected virtual void Awake()
        {
            if (!_runOnAwake)
            {
                return;
            }

            if (_autoFrom)
            {
                PlayFrom();
            }
            else
            {
                Play();
            }
        }

        protected virtual void Start()
        {
            if (!_runOnStart)
            {
                return;
            }

            if (_autoFrom)
            {
                PlayFrom();
            }
            else
            {
                Play();
            }
        }

        protected virtual void OnEnable()
        {
            if (!_runOnEnable)
            {
                return;
            }

            if (_autoFrom)
            {
                PlayFrom();
            }
            else
            {
                Play();
            }
        }

        protected virtual void OnDisable()
        {
            _targetTransform.DOKill();
        }

        protected abstract void OnValidate();

        private void RunOnAwakeChanged()
        {
            if (!_runOnAwake)
            {
                return;
            }

            _runOnStart = !_runOnAwake;
            _runOnEnable = !_runOnAwake;
        }

        private void RunOnStartChanged()
        {
            if (!_runOnStart)
            {
                return;
            }

            _runOnAwake = !_runOnStart;
            _runOnEnable = !_runOnStart;
        }

        private void RunOnEnableChanged()
        {
            if (!_runOnEnable)
            {
                return;
            }

            _runOnAwake = !_runOnEnable;
            _runOnStart = !_runOnEnable;
        }

        protected abstract bool ValidateTarget(Transform target, out string errorMsg);

        protected TweenParams GetTweenParams()
        {
            return new TweenParams()
                .SetLoops(_loops, _loopType)
                .SetEase(_easeType)
                .SetDelay(_delay)
                .OnStart(_onStartEvent.Invoke)
                .OnComplete(_onCompleteEvent.Invoke);
        }

        public abstract Tweener CreateTween();

        public Tweener CreateTweenFrom()
        {
            return CreateTween().From();
        }

        public virtual UniTask PlayAsTask()
        {
            return CreateTween().Play().ToUniTask(cancellationToken: this.GetCancellationTokenOnDestroy());
        }

        public virtual UniTask PlayFromAsTask()
        {
            return CreateTweenFrom().Play().ToUniTask(cancellationToken: this.GetCancellationTokenOnDestroy());
        }

        [TitleGroup("Actions", Order = 200)]
        [Button(ButtonSizes.Medium, Icon = SdfIconType.Play, IconAlignment = IconAlignment.LeftEdge)]
        public virtual void Play()
        {
            CreateTween().Play();
        }

        [TitleGroup("Actions", Order = 200)]
        [Button(ButtonSizes.Medium, Icon = SdfIconType.Play, IconAlignment = IconAlignment.LeftEdge)]
        public virtual void PlayFrom()
        {
            CreateTweenFrom().Play();
        }
    }
}
#endif