using DG.Tweening;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace DoTween
{
    public class CanvasGroupFadeDoTween : BaseDoTween
    {
        [Title("Fade Settings")]
        [DarkBox]
        [PropertyOrder(10)]
        [SerializeField]
        private float _value;

        [HideInInspector]
        [SerializeField]
        private CanvasGroup _canvasGroup;

        protected override void OnValidate()
        {
            if (_targetTransform == null)
            {
                return;
            }
            
            _canvasGroup = _targetTransform.GetComponent<CanvasGroup>();
        }

        protected override bool ValidateTarget(Transform target, out string errorMsg)
        {
            if (target == null)
            {
                errorMsg = "Target cannot be null!";
                return false;
            }

            var canvasGroup = target.GetComponent<CanvasGroup>();

            if (canvasGroup == null)
            {
                errorMsg = "Target must have a CanvasGroup component!";
                return false;
            }

            errorMsg = string.Empty;
            return true;
        }

        public override Tweener CreateTween()
        {
            return _canvasGroup.DOFade(_value, _duration).SetAs(GetTweenParams());
        }
    }
}