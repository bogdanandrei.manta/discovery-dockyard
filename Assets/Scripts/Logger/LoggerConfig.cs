namespace Logger
{
    public class LoggerConfig
    {
        public bool EnableDebug { get; private set; } = true;
        public bool EnableInfo { get; private set; } = true;
        public bool EnableWarning { get; private set; } = true;
        public bool EnableError { get; private set; } = true;

        public LoggerConfig() { }

        public LoggerConfig(bool enableDebug, bool enableInfo, bool enableWarning, bool enableError)
        {
            EnableDebug = enableDebug;
            EnableInfo = enableInfo;
            EnableWarning = enableWarning;
            EnableError = enableError;
        }
    }
}