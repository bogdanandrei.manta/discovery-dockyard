using System;
using Cysharp.Threading.Tasks;
using DependencyInjection;
using DTT.Utils.Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Logger
{
    public class LoggerService : ILoggerService
    {
        private LoggerConfig _loggerConfig;
        
        private LoggerChannels _loggerChannels;
        
        public void Initialize(DependencyContainer container)
        {
            _loggerConfig = new LoggerConfig();
            
            LoadChannels();
        }
        
        public void SetLogConfig(LoggerConfig config)
        {
            _loggerConfig = config ?? throw new ArgumentNullException(nameof(config));
        }
        
        public void LogDebug(string message, string channel)
        {
            Log(message, channel);
        }
        
        public void LogInfo(string message, string channel)
        {
            Log(message, channel, LoggerLevel.INFO);
        }
        
        public void LogWarning(string message, string channel)
        {
            Log(message, channel, LoggerLevel.WARNING);
        }
        
        public void LogError(string message, string channel)
        {
            Log(message, channel, LoggerLevel.ERROR);
        }

        private void Log(string message, string channel, LoggerLevel level = LoggerLevel.DEBUG)
        {
            if (!ShouldLog(level) || !IsChannelEnabled(channel))
            {
                return;
            }
            
            var formattedMessage = $"[{channel.Bold().Color(_loggerChannels.ChannelNameColor)}] {message.Color(GetColorForChannel(channel))}";
            LogWithColor(formattedMessage, level);
        }

        private async void LoadChannels()
        {
            var handle = Addressables.LoadAssetAsync<LoggerChannels>("Logger");
            await handle;
            
            
            _loggerChannels = handle.Result;
        }

        private void LogWithColor(string message, LoggerLevel level)
        {
            switch (level)
            {
                case LoggerLevel.DEBUG:
                    Debug.Log(message);
                    break;
                case LoggerLevel.INFO:
                    Debug.Log(message);
                    break;
                case LoggerLevel.WARNING:
                    Debug.LogWarning(message);
                    break;
                case LoggerLevel.ERROR:
                    Debug.LogError(message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }

        private bool ShouldLog(LoggerLevel level)
        {
            return level switch
            {
                LoggerLevel.DEBUG => _loggerConfig.EnableDebug,
                LoggerLevel.INFO => _loggerConfig.EnableInfo,
                LoggerLevel.WARNING => _loggerConfig.EnableWarning,
                LoggerLevel.ERROR => _loggerConfig.EnableError,
                _ => true
            };
        }

        private Color GetColorForChannel(string channelName)
        {
            if (_loggerChannels.Channels.TryGetValue(channelName, out var channel))
            {
                return channel.Color;
            }
            
            Debug.LogWarning($"LoggerService - Channel not found: {channelName}");
            
            return _loggerChannels.DefaultColor;
        }
        
        private bool IsChannelEnabled(string channelName)
        {
            if (_loggerChannels.Channels.TryGetValue(channelName, out var channel))
            {
                return channel.IsEnabled;
            }
            
            Debug.LogWarning($"LoggerService - Channel not found: {channelName}");
            
            return false;
        }
    }
}