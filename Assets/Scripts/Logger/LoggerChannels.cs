using System;
using System.Collections.Generic;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Logger
{
    [CreateAssetMenu(fileName = "SO_LoggerChannels", menuName = "Scriptable Objects/Logger Service/New Logger Channels")]
    public class LoggerChannels : SerializedScriptableObject
    {
        [field: Title("Default Color")]
        [field: HideLabel]
        [field: DarkBox]
        [field: SerializeField]
        public Color DefaultColor { get; private set; }
        
        [field: Title("Channel Name Color")]
        [field: HideLabel]
        [field: DarkBox]
        [field: SerializeField]
        public Color ChannelNameColor { get; private set; }
        
        [field: Title("Channels")]
        [field: OdinSerialize]
        [field: HideLabel]
        [field: DictionaryDrawerSettings(KeyLabel = "Channel Name", ValueLabel = "Channel Settings", DisplayMode = DictionaryDisplayOptions.OneLine)]
        public Dictionary<string, LoggerChannel> Channels { get; private set; } = new();
    }

    [Serializable]
    [InlineProperty]
    [HideReferenceObjectPicker]
    public class LoggerChannel
    {
        [field: SerializeField]
        [field: DarkBox]
        public Color Color { get; private set; } = Color.white;

        [field: SerializeField]
        [field: DarkBox]
        [field: HideLabel]
#if UNITY_EDITOR
        [field: ValueDropdown(nameof(_isEnabledDropdown))]
#endif
        public bool IsEnabled { get; private set; }

#if UNITY_EDITOR
        private ValueDropdownList<bool> _isEnabledDropdown = new()
        {
            new ValueDropdownItem<bool>("ENABLED", true),
            new ValueDropdownItem<bool>("DISABLED", false)
        };
#endif
    }
}