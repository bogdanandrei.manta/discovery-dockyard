namespace Logger
{
    public enum LoggerLevel
    {
        DEBUG,
        INFO,
        WARNING,
        ERROR
    }
}