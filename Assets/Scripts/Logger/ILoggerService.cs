using DependencyInjection;

namespace Logger
{
    public interface ILoggerService : IService
    {
        void LogDebug(string message, string channel);
        void LogInfo(string message, string channel);
        void LogWarning(string message, string channel);
        void LogError(string message, string channel);
    }
}