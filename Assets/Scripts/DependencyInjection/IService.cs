namespace DependencyInjection
{
    public interface IService
    {
        void Initialize(DependencyContainer container);
    }
}