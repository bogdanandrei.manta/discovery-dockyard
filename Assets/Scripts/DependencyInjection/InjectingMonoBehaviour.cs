using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace DependencyInjection
{
    public abstract class InjectingMonoBehaviour : MonoBehaviour
    {
        protected virtual void Awake()
        {
            InjectDependencies();
        }

        private void InjectDependencies()
        {
            var fieldsAndPropertiesWithAttribute = GetType()
                .GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(member => member.MemberType is MemberTypes.Field or MemberTypes.Property)
                .Where(member => member.GetCustomAttributes(typeof(InjectAttribute), inherit: true).Any());
            
            foreach (var member in fieldsAndPropertiesWithAttribute)
            {
                IService resolvedService;
                
                switch (member)
                {
                    case PropertyInfo propertyInfo:
                        resolvedService = DependencyContainer.Instance.Resolve(propertyInfo.PropertyType);
                        propertyInfo.SetValue(this, resolvedService);
                        break;
                    case FieldInfo fieldInfo:
                        resolvedService = DependencyContainer.Instance.Resolve(fieldInfo.FieldType);
                        fieldInfo.SetValue(this, resolvedService);
                        break;
                }
            }
        }
    }
}