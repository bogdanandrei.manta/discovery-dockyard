using System;
using System.Collections.Generic;
using Addressable;
using Factions;
using Logger;
using Popups;
using UnityEngine;

namespace DependencyInjection
{
    public class DependencyContainer
    {
        private readonly Dictionary<Type, Func<IService>> _serviceFactories = new();
        private readonly Dictionary<Type, IService> _instances = new();

        private static DependencyContainer _instance;

        public static DependencyContainer Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                _instance = new DependencyContainer();

                return _instance;
            }
        }

        private DependencyContainer()
        {
            // Register your services here
            Register<ILoggerService>(() => new LoggerService());
            Register<IAddressableService>(() => new AddressableService());
            Register<IPopupService>(() => new PopupService());
            Register<IFactionsService>(() => new FactionsService());
        }

        private void Register<TInterface>(Func<TInterface> factory) where TInterface : IService
        {
            if (!_serviceFactories.ContainsKey(typeof(TInterface)))
            {
                _serviceFactories[typeof(TInterface)] = () => factory();
                
                return;
            }
            
            Debug.Log($"Interface {typeof(TInterface)} already registered.");
        }

        public IService Resolve(Type typeToResolve)
        {
            if(!_serviceFactories.TryGetValue(typeToResolve, out Func<IService> factory))
            {
                Debug.LogWarning($"No implementation found for interface {typeToResolve}");
                
                return default;
            }
            
            if (_instances.TryGetValue(typeToResolve, out var instance))
            {
                return instance;
            }
            
            instance = factory.Invoke();
            instance.Initialize(this);
            _instances[typeToResolve] = instance;
            
            return instance;
        }

        public TInterface Resolve<TInterface>() where TInterface : IService
        {
            return (TInterface)Resolve(typeof(TInterface));
        }
    }
}