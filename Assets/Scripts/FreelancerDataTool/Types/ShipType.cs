namespace FreelancerDataTool.Types
{
    public enum ShipType
    {
        UNKNOWN,
        FIGHTER,
        FREIGHTER,
        TRANSPORT,
        GUNBOAT,
        CAPITAL,
    }
}