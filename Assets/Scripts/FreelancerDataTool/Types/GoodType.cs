namespace FreelancerDataTool.Types
{
    public enum GoodType
    {
        UNKNOWN,
        COMMODITY,
        COUNTER_MEASURE,
        MINE,
        SHIELD,
        THRUSTER,
        WEAPON
    }
}