namespace FreelancerDataTool.Types
{
    public enum ShipClass
    {
        UNKNOWN = -1,
        LIGHT_FIGHTER = 0,
        HEAVY_FIGHTER = 1,
        FREIGHTER = 2,
        VERY_HEAVY_FIGHTER = 3,
        BOMBER = 4,
        TRANSPORT = 6,
        TRAIN = 7,
        HEAVY_TRANSPORT = 8,
        LINER = 10,
        GUNBOAT = 11,
        CRUISER = 13,
        BATTLECRUISER = 15,
        BATTLESHIP = 16,
        DREADNOUGHT = 18,
    }
}