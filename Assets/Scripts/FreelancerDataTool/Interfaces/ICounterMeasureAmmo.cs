namespace FreelancerDataTool.Interfaces
{
    public interface ICounterMeasureAmmo : IAmmo
    {
        int Range { get; }
        int DiversionPercent { get; }
    }
}