namespace FreelancerDataTool.Interfaces
{
    public interface IThruster : IEquipment
    {
        float PowerUsage { get; }
        int MaxForce { get; }
    }
}