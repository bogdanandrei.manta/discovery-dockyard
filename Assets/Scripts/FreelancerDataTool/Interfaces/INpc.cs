using System;
using System.Collections.Generic;

namespace FreelancerDataTool.Interfaces
{
    public interface INpc : IItem
    {
        string Room { get; }
        IBase Base { get; }
        IFaction Affiliation { get; }
        
        Dictionary<IFaction, int> Bribes { get; }
        List<IInfocard> Rumors { get; }
    }
}