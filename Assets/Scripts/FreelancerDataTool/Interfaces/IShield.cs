namespace FreelancerDataTool.Interfaces
{
    public interface IShield : IGood
    {
        public int HitPoints { get; }
        public float ExplosionResistance { get; }
        public float RegenerationRate { get; }
        public float MaxCapacity { get; }
        public float OfflineRebuildTime { get; }
        public float OfflineThreshold { get; }
        public int ConstantPowerDraw { get; }
        public int RebuildPowerDraw { get; }
    }
}