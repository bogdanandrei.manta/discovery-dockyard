namespace FreelancerDataTool.Interfaces
{
    public interface IAmmo : IEquipment
    {
        float Lifetime { get; }
        int AmmoLimit { get; }
        bool RequiresAmmo { get; }
    }
}