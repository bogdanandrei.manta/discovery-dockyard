using System.Collections.Generic;
using UnityEngine;

namespace FreelancerDataTool.Interfaces
{
    public interface IFaction : IItem
    {
        IInfocard ShortName { get; }

        string Legality { get; }
        string JumpPreference { get; }

        Sprite Logo { get; }

        Dictionary<IFaction, float> Reputations { get; }
        Dictionary<INpc, float> NpcBribers { get; }

        List<IShip> ShipsUsed { get; }
        List<IBase> OwnedBases { get; }
        List<INpc> Npcs { get; }

        float ScanChance { get; }
        bool ScanAnnounce { get; }
    }
}