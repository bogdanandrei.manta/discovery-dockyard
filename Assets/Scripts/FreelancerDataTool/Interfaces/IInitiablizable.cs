namespace FreelancerDataTool.Interfaces
{
    public interface IInitializable <T>
    {
        void Initialize(T data);
    }
}