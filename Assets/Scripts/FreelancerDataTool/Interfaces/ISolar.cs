using System;

namespace FreelancerDataTool.Interfaces
{
    public interface ISolar : IItem
    {
        public Tuple<float, float, float> Position { get; }
        public Tuple<float, float, float> Rotation { get; }
        public string Archetype { get; }

    }
}