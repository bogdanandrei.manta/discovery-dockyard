namespace FreelancerDataTool.Interfaces
{
    public interface IEquipment : IGood
    {
        int HitPoints { get; }
        float ExplosionResistance { get; }
        int Mass { get; }
    }
}