using System;
using System.Collections.Generic;

namespace FreelancerDataTool.Interfaces
{
    public interface ISystem : IItem
    {
        string FilePath { get; }
        string Region { get; }
        Tuple<int, int> Position { get; }
        List<IBase> Bases { get; }
        float NavMapScale { get; }
    }
}