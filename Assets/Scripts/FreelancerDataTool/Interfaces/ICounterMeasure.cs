namespace FreelancerDataTool.Interfaces
{
    public interface ICounterMeasure : IEquipment
    {
        float PowerUsage { get; }
        float RefireDelay { get; }
        int MuzzleVelocity { get; }
        
        ICounterMeasureAmmo Ammo { get; }
    }
}