using System;
using FreelancerDataTool.Types;

namespace FreelancerDataTool.Interfaces
{
    public interface IShip : IGood
    {
        public float LinearDrag { get; }
        public float NudgeForce { get; }
        public Tuple<float, float, float> SteeringTorque { get; }
        public Tuple<float, float, float> AngularDrag { get; }
        public Tuple<float, float, float> RotationInertia { get; }
        public int HoldSize { get; }
        public int Mass { get; }
        public int StrafeForce { get; }
        public int StrafePowerUsage { get; }
        public int MaxBankAngle { get; }
        public int HitPoints { get; }
        public int Nanobots { get; }
        public int ShieldBats { get; }

        public ShipClass ShipClass { get; }
        public ShipType ShipType { get; }
    }
}