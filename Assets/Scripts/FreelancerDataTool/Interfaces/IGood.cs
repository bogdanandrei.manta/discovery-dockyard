using FreelancerDataTool.Types;

namespace FreelancerDataTool.Interfaces
{
    public interface IGood : IItem
    {
        public float Volume { get;  }
        public decimal PriceModifier { get; }
        public float BasePrice { get; }
        public GoodType GoodType { get; }
    }
}