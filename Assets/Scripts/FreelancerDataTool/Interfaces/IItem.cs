namespace FreelancerDataTool.Interfaces
{
    public interface IItem
    {
        public string Nickname { get; }
        
        public IInfocard Name { get; }
        public IInfocard Infocard { get; }
    }
}