namespace FreelancerDataTool.Interfaces
{
    public interface IBase : ISolar
    {
        ISystem System { get; }
        IFaction Reputation { get; }
        string FilePath { get; }
    }
}