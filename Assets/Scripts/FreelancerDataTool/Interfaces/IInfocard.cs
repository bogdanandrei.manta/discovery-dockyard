namespace FreelancerDataTool.Interfaces
{
    public interface IInfocard
    {
        public string Value { get; }
        public uint Key { get; }
    }
}