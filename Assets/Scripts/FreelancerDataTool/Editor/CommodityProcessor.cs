using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using FreelancerDataTool.Models;
using FreelancerDataTool.ScriptableObjects;
using FreelancerDataTool.Types;
using UnityEngine;

namespace FreelancerDataTool
{
    public class CommodityProcessor
    {
        private readonly FreelancerDataImporter _dataImporter;

        private readonly List<string> _commoditiesToIgnore = new()
        {
            "commodity_serverrules",
            "commodity_pvpserverrules",
            "commodity_textberne",
            "commodity_event_ore_02"
        };

        private readonly Dictionary<string, CommodityDto> _commodities = new();

        public CommodityProcessor(FreelancerDataImporter dataImporter)
        {
            _dataImporter = dataImporter;
        }

        private void ProcessSelectEquip()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(ProcessSelectEquip)}] Parsing select_equip.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.EQUIPMENT, "select_equip.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                if (section.Name.ToLower() != "commodity")
                {
                    continue;
                }

                var commodity = new CommodityDto
                {
                    GoodType = GoodType.COMMODITY
                };

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_commoditiesToIgnore.Contains(parameter.Value))
                            {
                                continue;
                            }

                            commodity.Nickname = parameter.Value;
                            break;
                        case "ids_name":
                            commodity.Name = new InfocardDto
                                { Key = uint.Parse(parameter.Value) };
                            break;
                        case "ids_info":
                            commodity.Infocard = new InfocardDto
                                { Key = uint.Parse(parameter.Value) };
                            break;
                        case "volume":
                            commodity.Volume = float.Parse(parameter.Value);
                            break;
                    }
                    
                    if (string.IsNullOrEmpty(commodity.Nickname) || string.IsNullOrWhiteSpace(commodity.Nickname))
                    {
                        continue;
                    }

                    _commodities[commodity.Nickname] = commodity;
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(ProcessSelectEquip)}] select_equip.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ProcessGoods()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(ProcessGoods)}] Parsing goods.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.EQUIPMENT, "goods.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                if (section.Name.ToLower() != "commodity")
                {
                    continue;
                }

                var parameters = section.GetParameters();

                if (parameters.Find(x => x.Key == "category")?.Value != "commodity")
                {
                    continue;
                }

                var commodity = new CommodityDto();

                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_commoditiesToIgnore.Contains(parameter.Value))
                            {
                                continue;
                            }

                            commodity.Nickname = parameter.Value;
                            break;
                        case "price":
                            commodity.BasePrice = float.Parse(parameter.Value);
                            break;
                    }

                    if (string.IsNullOrEmpty(commodity.Nickname) || string.IsNullOrWhiteSpace(commodity.Nickname))
                    {
                        continue;
                    }

                    if (_commodities.TryGetValue(commodity.Nickname, out var existingCommodity))
                    {
                        existingCommodity.BasePrice = commodity.BasePrice;
                    }
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(ProcessGoods)}] goods.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void AddInfocards()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(AddInfocards)}] Adding infocards to commodities...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var commodity in _commodities.Values)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(commodity.Name.Key);
                var infocard = _dataImporter.InfocardProcessor.GetInfocard(commodity.Infocard.Key);

                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(CommodityProcessor)}] [{nameof(AddInfocards)}] Name not found for commodity {commodity.Nickname}.", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                if (infocard is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(CommodityProcessor)}] [{nameof(AddInfocards)}] Infocard not found for commodity {commodity.Nickname}.", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                commodity.Name = name;
                commodity.Infocard = infocard;
            }

            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(AddInfocards)}] Infocards added to commodities successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void CreateOrUpdateScriptableObjects()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Creating or updating commodity scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var commodity in _commodities.Values)
            {
                string path = _dataImporter.GetScriptableObjectPath(commodity.Name.Value, FreelancerDataImporter.COMMODITY);

                if (FreelancerDataImporter.TryGetScriptableObject(path, out CommoditySO commoditySO))
                {
                    commoditySO.Initialize(commodity);
                    continue;
                }

                commoditySO = ScriptableObject.CreateInstance<CommoditySO>();
                commoditySO.Initialize(commodity);
                
                _dataImporter.CreateSO(commoditySO, path);
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Commodity scriptable objects created or updated successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        public UniTask Load()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] Loading commodities...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            ProcessSelectEquip();
            ProcessGoods();

            AddInfocards();

            CreateOrUpdateScriptableObjects();

            _dataImporter.Logger.LogInfo($"[{nameof(CommodityProcessor)}] Commodities loaded successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
            return UniTask.CompletedTask;
        }
    }
}