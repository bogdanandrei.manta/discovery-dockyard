using Factions;
using FreelancerDataTool.ScriptableObjects;
using Sirenix.OdinInspector.Editor;
using UnityEditor;

namespace FreelancerDataTool
{
    public class FreelancerDataTool : OdinMenuEditorWindow
    {
        public const string FREELANCER_DATA_TOOL = "Freelancer Data Tool";
        
        [MenuItem("Tools/Freelancer Data Tool")]
        private static void OpenWindow()
        {
            GetWindow<FreelancerDataTool>().Show();
        }
        
        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree { { "Data Importer", new FreelancerDataImporter() } };

            tree.AddAllAssetsAtPath("Factions", "Assets/ScriptableObjects/Factions", typeof(FactionSO));
            tree.AddAllAssetsAtPath("Ships", "Assets/ScriptableObjects/Ships", typeof(ShipSO));

            return tree;
        }
    }
}