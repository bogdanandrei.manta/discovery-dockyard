using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Cysharp.Threading.Tasks;
using DependencyInjection;
using FreelancerDataTool.Interfaces;
using JetBrains.Annotations;
using Logger;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Yuu.Ini;

namespace FreelancerDataTool
{
    public class FreelancerDataImporter
    {
        private string _freelancerPathErrorMessage;

        [Title("Freelancer Path", "Root path to Freelancer installation")]
        [FolderPath(AbsolutePath = true, RequireExistingPath = true)]
        [HideLabel]
        [ValidateInput(nameof(ValidateFreelancerPath), nameof(_freelancerPathErrorMessage))]
        [ShowInInspector]
        [DarkBox]
        public string FreelancerPath { get; set; }

        [Title("Scriptable Object's Folders Paths")]
        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Infocards")]
        [ShowInInspector]
        [DarkBox]
        public string InfocardsPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Systems")]
        [ShowInInspector]
        [DarkBox]
        public string SystemsPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Factions")]
        [ShowInInspector]
        [DarkBox]
        public string FactionsPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Ships")]
        [ShowInInspector]
        [DarkBox]
        public string ShipsPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Bases")]
        [ShowInInspector]
        [DarkBox]
        public string BasesPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Commodities")]
        [ShowInInspector]
        [DarkBox]
        public string CommoditiesPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Thrusters")]
        [ShowInInspector]
        [DarkBox]
        public string ThrustersPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Shields")]
        [ShowInInspector]
        [DarkBox]
        public string ShieldsPath { get; set; }

        [FolderPath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveScriptableObjectPaths))]
        [LabelWidth(70)]
        [LabelText("Npcs")]
        [ShowInInspector]
        [DarkBox]
        public string NpcsPath { get; set; }

        //TODO: Remove once infocards are loaded from dll
        [Title("Infocards JSON Path", "Path to the JSON file containing infocards data")]
        [HideLabel]
        [Sirenix.OdinInspector.FilePath(RequireExistingPath = true)]
        [OnValueChanged(nameof(SaveInfocardJsonPath))]
        [ShowInInspector]
        [DarkBox]
        public string InfocardsJsonPath { get; set; }

        private static readonly StringBuilder StringBuilder = new();

        public const string DATA = "DATA";
        public const string MISSIONS = "MISSIONS";
        public const string UNIVERSE = "UNIVERSE";
        public const string EQUIPMENT = "EQUIPMENT";
        public const string SHIPS = "SHIPS";
        public const string FACTION = "Faction";
        public const string SHIP = "Ship";
        public const string INFOCARD = "Infocard";
        public const string SYSTEM = "System";
        public const string BASE = "Base";
        public const string COMMODITY = "Commodity";
        public const string THRUSTER = "Thruster";
        public const string SHIELD = "Shield";
        public const string NPC = "Npc";

        private const string PATTERN = "[\\/:*?\"<>|]";

        public ILoggerService Logger { get; }
        public FactionProcessor FactionProcessor { get; }
        public InfocardProcessor InfocardProcessor { get; }
        public ShipProcessor ShipProcessor { get; }
        public BaseAndSystemProcessor BaseAndSystemProcessor { get; }
        public CommodityProcessor CommodityProcessor { get; }
        public ThrusterProcessor ThrusterProcessor { get; }
        public ShieldProcessor ShieldProcessor { get; }

        public FreelancerDataImporter()
        {
            FreelancerPath = EditorPrefs.GetString(nameof(FreelancerPath), string.Empty);
            InfocardsPath = EditorPrefs.GetString(nameof(InfocardsPath), string.Empty);
            SystemsPath = EditorPrefs.GetString(nameof(SystemsPath), string.Empty);
            FactionsPath = EditorPrefs.GetString(nameof(FactionsPath), string.Empty);
            ShipsPath = EditorPrefs.GetString(nameof(ShipsPath), string.Empty);
            BasesPath = EditorPrefs.GetString(nameof(BasesPath), string.Empty);
            CommoditiesPath = EditorPrefs.GetString(nameof(CommoditiesPath), string.Empty);
            ThrustersPath = EditorPrefs.GetString(nameof(ThrustersPath), string.Empty);
            ShieldsPath = EditorPrefs.GetString(nameof(ShieldsPath), string.Empty);
            NpcsPath = EditorPrefs.GetString(nameof(NpcsPath), string.Empty);

            //TODO: Remove once infocards are loaded from dll
            InfocardsJsonPath = EditorPrefs.GetString("InfocardsJsonPath", string.Empty);

            Logger = DependencyContainer.Instance.Resolve<ILoggerService>();
            FactionProcessor = new FactionProcessor(this);
            InfocardProcessor = new InfocardProcessor(this);
            ShipProcessor = new ShipProcessor(this);
            BaseAndSystemProcessor = new BaseAndSystemProcessor(this);
            CommodityProcessor = new CommodityProcessor(this);
            ThrusterProcessor = new ThrusterProcessor(this);
            ShieldProcessor = new ShieldProcessor(this);
        }

        private bool ValidateFreelancerPath(string path, ref string errorMessage)
        {
            if (string.IsNullOrEmpty(path))
            {
                errorMessage = "Path cannot be empty";
                return false;
            }

            if (!Directory.Exists(path))
            {
                errorMessage = "Path does not exist";
                return false;
            }

            if (!Directory.Exists(Path.Combine(FreelancerPath, "DATA")))
            {
                errorMessage = "Path does not contain Freelancer DATA folder";
                return false;
            }

            EditorPrefs.SetString("FreelancerPath", path);

            return true;
        }

        private void SaveScriptableObjectPaths()
        {
            if (!string.IsNullOrEmpty(InfocardsPath) && !string.IsNullOrWhiteSpace(InfocardsPath))
            {
                EditorPrefs.SetString(nameof(InfocardsPath), InfocardsPath);
            }

            if (!string.IsNullOrEmpty(SystemsPath) && !string.IsNullOrWhiteSpace(SystemsPath))
            {
                EditorPrefs.SetString(nameof(SystemsPath), SystemsPath);
            }

            if (!string.IsNullOrEmpty(FactionsPath) && !string.IsNullOrWhiteSpace(FactionsPath))
            {
                EditorPrefs.SetString(nameof(FactionsPath), FactionsPath);
            }

            if (!string.IsNullOrEmpty(ShipsPath) && !string.IsNullOrWhiteSpace(ShipsPath))
            {
                EditorPrefs.SetString(nameof(ShipsPath), ShipsPath);
            }

            if (!string.IsNullOrEmpty(BasesPath) && !string.IsNullOrWhiteSpace(BasesPath))
            {
                EditorPrefs.SetString(nameof(BasesPath), BasesPath);
            }

            if (!string.IsNullOrEmpty(CommoditiesPath) && !string.IsNullOrWhiteSpace(CommoditiesPath))
            {
                EditorPrefs.SetString(nameof(CommoditiesPath), CommoditiesPath);
            }

            if (!string.IsNullOrEmpty(ThrustersPath) && !string.IsNullOrWhiteSpace(ThrustersPath))
            {
                EditorPrefs.SetString(nameof(ThrustersPath), ThrustersPath);
            }

            if (!string.IsNullOrEmpty(ShieldsPath) && !string.IsNullOrWhiteSpace(ShieldsPath))
            {
                EditorPrefs.SetString(nameof(ShieldsPath), ShieldsPath);
            }

            if (!string.IsNullOrEmpty(NpcsPath) && !string.IsNullOrWhiteSpace(NpcsPath))
            {
                EditorPrefs.SetString(nameof(NpcsPath), NpcsPath);
            }
        }

        //TODO: Remove once infocards are loaded from dll
        private void SaveInfocardJsonPath()
        {
            if (!string.IsNullOrEmpty(InfocardsJsonPath) && !string.IsNullOrWhiteSpace(InfocardsJsonPath))
            {
                EditorPrefs.SetString("InfocardsJsonPath", InfocardsJsonPath);
            }
        }

        [Title("Actions")]
        [Button(ButtonSizes.Large, Icon = SdfIconType.ArrowDown, IconAlignment = IconAlignment.RightEdge)]
        [UsedImplicitly]
        private async void ImportData()
        {
            var tasks = new List<UniTask>
            {
                InfocardProcessor.Load(),
                FactionProcessor.Load(),
                ShipProcessor.Load(),
                BaseAndSystemProcessor.Load(),
                // CommodityProcessor.Load(),
                // ThrusterProcessor.Load(),
                // ShieldProcessor.Load()
            };

            await UniTask.WhenAll(tasks);

            FactionProcessor.AddAdditionalData();
            BaseAndSystemProcessor.AddAdditionalData();

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Logger.LogInfo($"[{nameof(FreelancerDataImporter)}] [{nameof(ImportData)}] Data imported successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private static string FormatName(string path)
        {
            StringBuilder.Clear();

            string[] split = path.Split('/');
            for (int i = 0; i < split.Length; i++)
            {
                if (i == split.Length - 1)
                {
                    string formatted = split[i].Replace(" ", "").Replace("\n", "");
                    formatted = Regex.Replace(formatted, PATTERN, string.Empty);

                    StringBuilder.Append(formatted);
                    continue;
                }

                StringBuilder.Append(split[i]);
                StringBuilder.Append("/");
            }

            return StringBuilder.ToString();
        }

        public IniDocument ReadIniFile(string path)
        {
            try
            {
                using var fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using var sr = new StreamReader(fs, Encoding.ASCII);
                return IniParser.Parse(sr.ReadToEnd());
            }
            catch (Exception e)
            {
                Logger.LogError($"[{nameof(ReadIniFile)}] {e.Message}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                return null;
            }
        }

        public string GetScriptableObjectPath(string searchFor, string processor)
        {
            if (!uint.TryParse(searchFor, out _))
            {
                searchFor = FormatName(searchFor);
            }

            return processor switch
            {
                INFOCARD => Path.Combine(InfocardsPath, $"SO_Infocard_{searchFor}.asset"),
                SHIP => Path.Combine(ShipsPath, $"SO_Ship_{searchFor.Replace(" ", "")}.asset"),
                FACTION => Path.Combine(FactionsPath, $"SO_Faction_{searchFor.Replace(" ", "")}.asset"),
                SYSTEM => Path.Combine(SystemsPath, $"SO_System_{searchFor.Replace(" ", "")}.asset"),
                BASE => Path.Combine(BasesPath, $"SO_Base_{searchFor.Replace(" ", "")}.asset"),
                COMMODITY => Path.Combine(CommoditiesPath, $"SO_Commodity_{searchFor.Replace(" ", "")}.asset"),
                THRUSTER => Path.Combine(ThrustersPath, $"SO_Thruster_{searchFor.Replace(" ", "")}.asset"),
                SHIELD => Path.Combine(ShieldsPath, $"SO_Shield_{searchFor.Replace(" ", "")}.asset"),
                NPC => Path.Combine(NpcsPath, $"SO_Npc_{searchFor.Replace(" ", "")}.asset"),
                _ => string.Empty
            };
        }

        public void CreateSO<T>(T scriptableObject, string path) where T : ScriptableObject, IItem
        {
            try
            {
                AssetDatabase.CreateAsset(scriptableObject, path);
            }
            catch (Exception e)
            {
                Logger.LogError($"[{nameof(CreateSO)}] Error creating so for {scriptableObject.Nickname}:{scriptableObject.Name.Key} {e.Message}", FreelancerDataTool.FREELANCER_DATA_TOOL);
            }
        }

        public static bool TryGetScriptableObject<T>(string soPath, out T scriptableObject) where T : ScriptableObject
        {
            soPath = FormatName(soPath);
            scriptableObject = AssetDatabase.LoadAssetAtPath<T>(soPath);
            return scriptableObject is not null;
        }
    }
}