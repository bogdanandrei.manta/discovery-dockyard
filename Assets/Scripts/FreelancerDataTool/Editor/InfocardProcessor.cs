using System.Collections.Generic;
using System.Text;
using Cysharp.Threading.Tasks;
using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Models;
using FreelancerDataTool.ScriptableObjects;
using Sirenix.Utilities;
using Unity.Plastic.Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

namespace FreelancerDataTool
{
    public class InfocardProcessor
    {
        private static readonly StringBuilder StringBuilder = new();
        
            private static Dictionary<string, string> _rdlToHtml = new()
    {
        {"<TRA data=\"1\" mask=\"1\" def=\"-2\"/>", "<b>"},  
        {"<TRA bold=\"true\"/>", "<b>"},
        {"<TRA data=\"0\" mask=\"1\" def=\"-1\"/>", "</b>"}, 
        {"<TRA data=\"0x00000001\" mask=\"-1\" def=\"-2\"/>", "<b>"},
        {"<TRA data=\"0x00000000\" mask=\"-1\" def=\"-1\"/>", "</b>"}, 
        {"<TRA data=\"2\" mask=\"3\" def=\"-3\"/>", "<i>"},
        {"<TRA data=\"0\" mask=\"3\" def=\"-1\"/>", "</i>"},  
        {"<TRA data=\"98\" mask=\"-29\" def=\"-3\"/>", "<i>"},  
        {"<TRA data=\"96\" mask=\"-29\" def=\"-1\"/>", "</i>"},  
        {"<TRA data=\"2\" mask=\"2\" def=\"-3\"/>", "<i>"},  
        {"<TRA data=\"0\" mask=\"2\" def=\"-1\"/>", "</i>"},  
        {"<TRA data=\"5\" mask=\"5\" def=\"-6\"/>", "<b><u>"},  
        {"<TRA data=\"0\" mask=\"5\" def=\"-1\"/>", "</b></u>"},  
        {"<TRA data=\"5\" mask=\"7\" def=\"-6\"/>", "<b><u>"},  
        {"<TRA data=\"0\" mask=\"7\" def=\"-1\"/>", "</b></u>"},  
        {"<TRA data=\"65280\" mask=\"-32\" def=\"31\"/>", "<font color=\"red\">"},  
        {"<TRA data=\"96\" mask=\"-32\" def=\"-1\"/>", "</font>"},  
        {"<TRA color=\"default\"/>", "</font>"},  
        {"<TRA data=\"65281\" mask=\"-31\" def=\"30\"/>", "<b><font color=\"red\">"},  
        {"<TRA data=\"96\" mask=\"-31\" def=\"-1\"/>", "</b></font>"},  
        {"<TRA data=\"-16777216\" mask=\"-32\" def=\"31\"/>", "<font color=\"blue\">"},  
        {"<TRA color=\"white\" bold=\"default\"/>", "</b><font color=\"white>"},
        {"<PARA/>", "<br>"},  
        {"</PARA>", ""},
        {"<JUST loc=\"left\"/>", "<p align=\"left\">"},
        {"<JUST loc=\"center\"/>", "<p align=\"center\">"},  
        {"\xa0", "<nbsp>"},  
        {"<RDL>", ""},
        {"</RDL>", ""},
        {"<TEXT>", ""},
        {"</TEXT>", ""},
        {"<PUSH/>", ""},
        {"<POP/>", ""},
        {"<?xml version=\"1.0\" encoding=\"UTF-16\"?>", ""} 
    };

        private readonly FreelancerDataImporter _dataImporter;
        private readonly Dictionary<uint, InfocardDto> _infocards = new();

        public InfocardProcessor(FreelancerDataImporter dataImporter)
        {
            _dataImporter = dataImporter;
        }

        private string FormatInfocard(string input)
        {
            StringBuilder.Clear();
            StringBuilder.Append(input);
            
            foreach (var pair in _rdlToHtml)
            {
                StringBuilder.Replace(pair.Key, pair.Value);
            }
            
            return StringBuilder.ToString();
        }

        public UniTask Load()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(Load)}] Loading infocards...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            if (string.IsNullOrEmpty(_dataImporter.InfocardsJsonPath) || string.IsNullOrWhiteSpace(_dataImporter.InfocardsJsonPath))
            {
                _dataImporter.Logger.LogError($"[{nameof(Load)}] Infocards json path is empty", FreelancerDataTool.FREELANCER_DATA_TOOL);
                return new UniTask();
            }

            string json = AssetDatabase.LoadAssetAtPath<TextAsset>(_dataImporter.InfocardsJsonPath)?.text;

            if (string.IsNullOrEmpty(json) || string.IsNullOrWhiteSpace(json))
            {
                return new UniTask();
            }

            _infocards.Clear();
            JsonConvert.DeserializeObject<Dictionary<uint, string>>(json).ForEach(x => _infocards.Add(x.Key, new InfocardDto() { Key = x.Key, Value = x.Value }));

            _dataImporter.Logger.LogInfo($"[{nameof(Load)}] Infocards loaded successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);

            return UniTask.CompletedTask;
        }

        public IInfocard GetInfocard(uint id)
        {
            string path = _dataImporter.GetScriptableObjectPath(id.ToString(), FreelancerDataImporter.INFOCARD);

            if (FreelancerDataImporter.TryGetScriptableObject(path, out InfocardSO so))
            {
                return so;
            }

            if (!_infocards.TryGetValue(id, out var infocard))
            {
                return null;
            }

            infocard.Value = FormatInfocard(infocard.Value);

            var soInstance = ScriptableObject.CreateInstance<InfocardSO>();
            soInstance.Initialize(infocard);

            AssetDatabase.CreateAsset(soInstance, path);
            AssetDatabase.SaveAssets();

            return soInstance;
        }
    }
}