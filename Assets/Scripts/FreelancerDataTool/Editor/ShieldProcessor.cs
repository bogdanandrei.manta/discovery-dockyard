using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using FreelancerDataTool.Models;
using FreelancerDataTool.ScriptableObjects;
using FreelancerDataTool.Types;
using UnityEngine;

namespace FreelancerDataTool
{
    public class ShieldProcessor
    {
        private readonly FreelancerDataImporter _dataImporter;

        private readonly List<string> _shieldsToIgnore = new();
        private readonly Dictionary<string, ShieldDto> _shields = new();

        public ShieldProcessor(FreelancerDataImporter dataImporter)
        {
            _dataImporter = dataImporter;
        }

        private void ParseStEquip()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(ParseStEquip)}] Parsing st_equip.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.EQUIPMENT, "st_equip.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                if (section.Name.ToLower() != "shieldgenerator")
                {
                    continue;
                }

                var shield = new ShieldDto()
                {
                    GoodType = GoodType.SHIELD
                };

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_shieldsToIgnore.Contains(parameter.Value))
                            {
                                continue;
                            }

                            shield.Nickname = parameter.Value;
                            break;
                        case "ids_name":
                            shield.Name = new InfocardDto
                                { Key = uint.Parse(parameter.Value) };
                            break;
                        case "ids_info":
                            shield.Infocard = new InfocardDto
                                { Key = uint.Parse(parameter.Value) };
                            break;
                        case "hit_points":
                            shield.HitPoints = int.Parse(parameter.Value);
                            break;
                        case "explosion_resistance":
                            shield.ExplosionResistance = float.Parse(parameter.Value);
                            break;
                        case "regeneration_rate":
                            shield.RegenerationRate = float.Parse(parameter.Value);
                            break;
                        case "max_capacity":
                            shield.MaxCapacity = float.Parse(parameter.Value);
                            break;
                        case "offline_rebuild_time":
                            shield.OfflineRebuildTime = float.Parse(parameter.Value);
                            break;
                        case "offline_threshold":
                            shield.OfflineThreshold = float.Parse(parameter.Value);
                            break;
                        case "constant_power_draw":
                            shield.ConstantPowerDraw = int.Parse(parameter.Value);
                            break;
                        case "rebuild_power_draw":
                            shield.RebuildPowerDraw = int.Parse(parameter.Value);
                            break;
                    }

                    if (string.IsNullOrEmpty(shield.Nickname) || string.IsNullOrWhiteSpace(shield.Nickname))
                    {
                        continue;
                    }

                    _shields[shield.Nickname] = shield;
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(ParseStEquip)}] st_equip.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ParseStGood()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(ParseStGood)}] Parsing st_good.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.EQUIPMENT, "st_good.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                var parameters = section.GetParameters();

                if (parameters.Find(x => x.Key == "nickname")?.Value?.Contains("shield") != true)
                {
                    continue;
                }

                var shield = new ShieldDto();
                
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_shieldsToIgnore.Contains(parameter.Value))
                            {
                                continue;
                            }

                            shield.Nickname = parameter.Value;
                            break;
                        case "price":
                            shield.BasePrice = float.Parse(parameter.Value);
                            break;
                    }
                    
                    if (string.IsNullOrEmpty(shield.Nickname) || string.IsNullOrWhiteSpace(shield.Nickname))
                    {
                        continue;
                    }
                    
                    if (_shields.TryGetValue(shield.Nickname, out var existingShield))
                    {
                        existingShield.BasePrice = shield.BasePrice;
                    }
                }
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(ParseStGood)}] st_good.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void AddInfocards()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(AddInfocards)}] Adding infocards to shields...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var shield in _shields.Values)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(shield.Name.Key);
                var infocard = _dataImporter.InfocardProcessor.GetInfocard(shield.Infocard.Key);
                
                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(ShieldProcessor)}] [{nameof(AddInfocards)}] Name not found for shield: {shield.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }
                
                if (infocard is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(ShieldProcessor)}] [{nameof(AddInfocards)}] Infocard not found for shield: {shield.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }
                
                shield.Name = name;
                shield.Infocard = infocard;
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(AddInfocards)}] Infocards added to shield successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void CreateOrUpdateScriptableObjects()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Creating shield scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var shield in _shields.Values)
            {
                string path = _dataImporter.GetScriptableObjectPath(shield.Name.Value, FreelancerDataImporter.SHIELD);
                
                if(FreelancerDataImporter.TryGetScriptableObject(path, out ShieldSO shieldSO))
                {
                    shieldSO.Initialize(shield);
                    continue;
                }
                
                shieldSO = ScriptableObject.CreateInstance<ShieldSO>();
                shieldSO.Initialize(shield);
                
                _dataImporter.CreateSO(shieldSO, path);
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Shield scriptable objects created successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        public UniTask Load()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(Load)}] Loading thrusters...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            ParseStEquip();
            ParseStGood();

            AddInfocards();

            CreateOrUpdateScriptableObjects();

            _dataImporter.Logger.LogInfo($"[{nameof(ShieldProcessor)}] [{nameof(Load)}] Thrusters loaded successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
            return UniTask.CompletedTask;
        }
    }
}