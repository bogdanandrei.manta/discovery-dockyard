using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cysharp.Threading.Tasks;
using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Models;
using FreelancerDataTool.ScriptableObjects;
using UnityEditor;
using UnityEngine;

namespace FreelancerDataTool
{
    public class BaseAndSystemProcessor
    {
        private readonly FreelancerDataImporter _dataImporter;

        private readonly List<string> _systemsToIgnore = new()
        {
            "hlp1",
            "hlp2",
            "sector01",
            "sector02",
            "sector03",
            "sector04"
        };

        private readonly Dictionary<string, BaseDto> _bases = new();
        private readonly Dictionary<string, SystemDto> _systems = new();
        private readonly Dictionary<string, NpcDto> _npcs = new();

        public Dictionary<string, IBase> BasesScriptableObjects { get; } = new();
        public Dictionary<string, ISystem> SystemsScriptableObjects { get; } = new();
        public Dictionary<string, INpc> NpcsScriptableObjects { get; } = new();

        public BaseAndSystemProcessor(FreelancerDataImporter dataImporter)
        {
            _dataImporter = dataImporter;
        }

        private void ParseUniverse()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(ParseUniverse)}] Parsing universe.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.UNIVERSE, "universe.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                _dataImporter.Logger.LogError($"[{nameof(BaseAndSystemProcessor)}] [{nameof(ParseUniverse)}] universe.ini not found", FreelancerDataTool.FREELANCER_DATA_TOOL);
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                if (section.Name.ToLower() != "system")
                {
                    continue;
                }

                var system = new SystemDto();

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_systemsToIgnore.Contains(parameter.Value))
                            {
                                continue;
                            }

                            system.Nickname = parameter.Value;
                            break;
                        case "strid_name":
                            system.Name = new InfocardDto
                            {
                                Key = uint.Parse(parameter.Value)
                            };
                            break;
                        case "file":
                            string[] pathElements = parameter.Value.Split('\\');

                            system.FilePath = Path.Combine(pathElements);
                            break;
                        case "ids_info":
                            system.Infocard = new InfocardDto
                            {
                                Key = uint.Parse(parameter.Value)
                            };
                            break;
                    }

                    if (!string.IsNullOrEmpty(system.Nickname) && !string.IsNullOrWhiteSpace(system.Nickname))
                    {
                        _systems[system.Nickname] = system;
                    }
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(ParseUniverse)}] universe.ini parsed successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ParseSystemsFiles()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(ParseSystemsFiles)}] Parsing system files...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var system) in _systems)
            {
                string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.UNIVERSE, system.FilePath);

                var ini = _dataImporter.ReadIniFile(iniPath);

                if (ini is null)
                {
                    continue;
                }

                var sections = ini.GetSections();
                foreach (var section in sections)
                {
                    if (section.Name.ToLower() != "object")
                    {
                        continue;
                    }

                    var parameters = section.GetParameters();

                    if (parameters.All(x => x.Key != "base"))
                    {
                        continue;
                    }

                    var @base = new BaseDto()
                    {
                        SystemNickname = system.Nickname
                    };

                    foreach (var parameter in parameters)
                    {
                        switch (parameter.Key)
                        {
                            case "nickname":
                                @base.Nickname = parameter.Value;
                                break;
                            case "ids_name":
                                @base.Name = new InfocardDto
                                {
                                    Key = uint.Parse(parameter.Value)
                                };
                                break;
                            case "ids_info":
                                @base.Infocard = new InfocardDto
                                {
                                    Key = uint.Parse(parameter.Value)
                                };
                                break;
                            case "pos":
                                string[] pos = parameter.Value.Replace(" ", "").Split(',');
                                @base.Position = new Tuple<float, float, float>(float.Parse(pos[0]), float.Parse(pos[1]), float.Parse(pos[2]));
                                break;
                            case "rotate":
                                string[] rotate = parameter.Value.Replace(" ", "").Split(',');
                                @base.Rotation = new Tuple<float, float, float>(float.Parse(rotate[0]), float.Parse(rotate[1]), float.Parse(rotate[2]));
                                break;
                            case "base":
                                @base.Base = parameter.Value;
                                break;
                            case "reputation":
                                @base.ReputationNickname = parameter.Value;
                                break;
                        }
                    }

                    if (@base.Nickname.Contains("proxy"))
                    {
                        continue;
                    }

                    if (@base.Nickname + "_Base" != @base.Base)
                    {
                        continue;
                    }

                    @base.Nickname = @base.Base;
                    @base.System = system;

                    system.Bases ??= new List<IBase>();
                    if (system.Bases.All(x => x.Nickname != @base.Nickname))
                    {
                        system.Bases.Add(@base);
                    }


                    _bases[@base.Nickname] = @base;
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(ParseSystemsFiles)}] System files parsed successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ParseMBases()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(ParseMBases)}] Parsing m_bases.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.MISSIONS, "mbases.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            BaseDto @base = null;
            NpcDto npc = null;

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            switch (section.Name.ToLower())
                            {
                                case "mbase":
                                    if (_bases.TryGetValue(parameter.Value, out var storedBase))
                                    {
                                        @base = storedBase;
                                    }
                                    break;
                                case "gf_npc":
                                    npc = new NpcDto
                                    {
                                        Nickname = parameter.Value,
                                        BaseNickname = @base?.Nickname
                                    };
                                    break;
                            }
                            break;
                        case "faction":
                            @base?.InhabitingFactionsNicknames.Add(parameter.Value);
                            break;
                        case "npc":
                            @base?.NpcNicknames.Add(parameter.Value);
                            break;
                        case "affiliation":
                            if (npc != null)
                            {
                                npc.AffiliationNickname = parameter.Value;
                            }
                            break;
                        case "individual_name":
                            if (npc != null)
                            {
                                npc.Name = new InfocardDto
                                {
                                    Key = uint.Parse(parameter.Value)
                                };
                            }
                            break;
                        case "bribe":
                            var bribeSplit = parameter.Value.Replace(" ", "").Split(',');
                            if (npc != null)
                            {
                                npc.BribesNicknames[bribeSplit[0]] = int.Parse(bribeSplit[1]);
                            }
                            break;
                        case "rumor":
                            var rumorSplit = parameter.Value.Replace(" ", "").Split(',');
                            npc?.Rumors.Add(new InfocardDto
                            {
                                Key = uint.Parse(rumorSplit[^1])
                            });
                            break;
                        case "fixture":
                            var split = parameter.Value.Replace(" ", "").Split(',');
                            @base?.NpcNicknames.Add(split[0]);
                            break;
                        case "room":
                            if (npc != null)
                            {
                                npc.Room = parameter.Value;
                            }
                            break;
                    }
                }

                if (npc is null)
                {
                    continue;
                }

                _npcs[npc.Nickname] = npc;
                npc = null;
            }
        }

        private void AddInfocards()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AddInfocards)}] Adding infocards to bases and systems...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var @base) in _bases)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(@base.Name.Key);
                var infocard = _dataImporter.InfocardProcessor.GetInfocard(@base.Infocard.Key);

                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AddInfocards)}] Name not found for base: {@base.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                if (infocard is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AddInfocards)}] Infocard not found for base: {@base.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                @base.Name = name;
                @base.Infocard = infocard;
            }

            foreach ((string _, var system) in _systems)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(system.Name.Key);
                var infocard = _dataImporter.InfocardProcessor.GetInfocard(system.Infocard.Key);

                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AddInfocards)}] Name not found for system: {system.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                if (infocard is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AddInfocards)}] Infocard not found for system: {system.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                system.Name = name;
                system.Infocard = infocard;
            }

            foreach (var npc in _npcs.Values)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(npc.Name.Key);

                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AddInfocards)}] Name not found for npc: {npc.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                var tempRumors = new List<IInfocard>();
                foreach (var npcRumor in npc.Rumors)
                {
                    var rumor = _dataImporter.InfocardProcessor.GetInfocard(npcRumor.Key);

                    if (rumor is null)
                    {
                        _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AddInfocards)}] Rumor not found for npc: {npcRumor.Key}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                        continue;
                    }

                    tempRumors.Add(rumor);
                }

                npc.Name = name;
                npc.Rumors = tempRumors;
            }
        }

        private void CreateScriptableObjects()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(CreateScriptableObjects)}] Creating system scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var system) in _systems)
            {
                string path = _dataImporter.GetScriptableObjectPath(system.Name.Value, FreelancerDataImporter.SYSTEM);

                if (FreelancerDataImporter.TryGetScriptableObject(path, out SystemSO systemSO))
                {
                    systemSO.Initialize(system);

                    SystemsScriptableObjects[system.Nickname] = systemSO;

                    continue;
                }

                systemSO = ScriptableObject.CreateInstance<SystemSO>();
                systemSO.Initialize(system);

                SystemsScriptableObjects[system.Nickname] = systemSO;

                _dataImporter.CreateSO(systemSO, path);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(CreateScriptableObjects)}] System scriptable objects created successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(CreateScriptableObjects)}] Creating base scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var @base) in _bases)
            {
                string path = _dataImporter.GetScriptableObjectPath(@base.Name.Value, FreelancerDataImporter.BASE);

                if (FreelancerDataImporter.TryGetScriptableObject(path, out BaseSO baseSO))
                {
                    baseSO.Initialize(@base);

                    BasesScriptableObjects[@base.Nickname] = baseSO;

                    continue;
                }

                baseSO = ScriptableObject.CreateInstance<BaseSO>();
                baseSO.Initialize(@base);

                BasesScriptableObjects[@base.Nickname] = baseSO;

                _dataImporter.CreateSO(baseSO, path);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(CreateScriptableObjects)}] Base scriptable objects created successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(CreateScriptableObjects)}] Creating npc scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var npc in _npcs.Values)
            {
                string path = _dataImporter.GetScriptableObjectPath(npc.Name.Value, FreelancerDataImporter.NPC);

                if (FreelancerDataImporter.TryGetScriptableObject(path, out NpcSO npcSO))
                {
                    npcSO.Initialize(npc);

                    NpcsScriptableObjects[npc.Nickname] = npcSO;

                    continue;
                }

                npcSO = ScriptableObject.CreateInstance<NpcSO>();
                npcSO.Initialize(npc);

                NpcsScriptableObjects[npc.Nickname] = npcSO;

                _dataImporter.CreateSO(npcSO, path);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(CreateScriptableObjects)}] Npc scriptable objects created successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private async void AssignSystemsAndBases()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignSystemsAndBases)}] Assigning systems and bases...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            var tasks = BasesScriptableObjects.Values.Cast<BaseSO>().Select(baseSO =>
            {
                var baseDto = _bases[baseSO.Nickname];

                if (SystemsScriptableObjects.TryGetValue(baseDto.SystemNickname, out var systemSO))
                {
                    baseDto.System = systemSO;

                    systemSO.Bases.Add(baseSO);

                    EditorUtility.SetDirty((SystemSO)systemSO);
                }
                else
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignSystemsAndBases)}] System {baseDto.SystemNickname} not found for base: {baseSO.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                }

                if (!string.IsNullOrEmpty(baseDto.ReputationNickname) && _dataImporter.FactionProcessor.FactionsScriptableObjects.TryGetValue(baseDto.ReputationNickname, out var factionSO))
                {
                    baseDto.Reputation = factionSO;

                    ((FactionSO)factionSO).OwnedBases.Add(baseSO);

                    EditorUtility.SetDirty((FactionSO)factionSO);
                }
                else
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignSystemsAndBases)}] Faction {baseDto.ReputationNickname} not found for base: {baseSO.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                }

                baseSO.Initialize(baseDto);

                EditorUtility.SetDirty(baseSO);
                
                return UniTask.CompletedTask;
            });

            await UniTask.WhenAll(tasks).ContinueWith(() =>
            {
                _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignSystemsAndBases)}] Systems and bases assigned successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
            });
        }

        private void AssignBaseAffiliationAndBribesToNpcs()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignBaseAffiliationAndBribesToNpcs)}] Assigning base affiliation and bribes to npcs...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var npcSO in NpcsScriptableObjects.Values.Cast<NpcSO>())
            {
                var npcDto = _npcs[npcSO.Nickname];

                if (!string.IsNullOrEmpty(npcDto.BaseNickname) && BasesScriptableObjects.TryGetValue(npcDto.BaseNickname, out var baseSO))
                {
                    npcDto.Base = baseSO;
                }
                else
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignBaseAffiliationAndBribesToNpcs)}] Base {npcDto.BaseNickname} not found for npc: {npcSO.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                }

                if (!string.IsNullOrEmpty(npcDto.AffiliationNickname) && _dataImporter.FactionProcessor.FactionsScriptableObjects.TryGetValue(npcDto.AffiliationNickname, out var affiliationSO))
                {
                    npcDto.Affiliation = affiliationSO;

                    ((FactionSO)affiliationSO).Npcs.Add(npcSO);

                    EditorUtility.SetDirty((FactionSO)affiliationSO);
                }
                else
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignBaseAffiliationAndBribesToNpcs)}] Faction {npcDto.AffiliationNickname} not found for npc: {npcSO.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                }

                foreach ((string bribeNickname, int price) in npcDto.BribesNicknames)
                {
                    if (_dataImporter.FactionProcessor.FactionsScriptableObjects.TryGetValue(bribeNickname, out var bribeSO))
                    {
                        npcDto.Bribes[bribeSO] = price;

                        ((FactionSO)bribeSO).NpcBribers[npcSO] = price;

                        EditorUtility.SetDirty((FactionSO)bribeSO);
                    }
                    else
                    {
                        _dataImporter.Logger.LogWarning($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignBaseAffiliationAndBribesToNpcs)}] Faction {bribeNickname} not found for npc: {npcSO.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    }
                }

                npcSO.Initialize(npcDto);

                EditorUtility.SetDirty(npcSO);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(AssignBaseAffiliationAndBribesToNpcs)}] Base affiliation and bribes assigned successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        public UniTask Load()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(Load)}] Loading bases and systems...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            ParseUniverse();
            ParseSystemsFiles();
            ParseMBases();

            AddInfocards();

            CreateScriptableObjects();

            _dataImporter.Logger.LogInfo($"[{nameof(BaseAndSystemProcessor)}] [{nameof(Load)}] Bases and systems loaded successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);

            return UniTask.CompletedTask;
        }

        public void AddAdditionalData()
        {
            AssignSystemsAndBases();
            AssignBaseAffiliationAndBribesToNpcs();
        }
    }
}