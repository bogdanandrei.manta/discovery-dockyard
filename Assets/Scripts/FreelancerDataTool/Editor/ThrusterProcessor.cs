using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cysharp.Threading.Tasks;
using FreelancerDataTool.Models;
using FreelancerDataTool.ScriptableObjects;
using FreelancerDataTool.Types;
using UnityEngine;

namespace FreelancerDataTool
{
    public class ThrusterProcessor
    {
        private readonly FreelancerDataImporter _dataImporter;

        private readonly List<string> _thrustersToIgnore = new();
        private readonly Dictionary<string, ThrusterDto> _thrusters = new();

        public ThrusterProcessor(FreelancerDataImporter dataImporter)
        {
            _dataImporter = dataImporter;
        }

        private void ParseStEquip()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(ParseStEquip)}] Parsing st_equip.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.EQUIPMENT, "st_equip.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                if (section.Name.ToLower() != "thruster")
                {
                    continue;
                }

                var thruster = new ThrusterDto
                {
                    GoodType = GoodType.THRUSTER
                };

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_thrustersToIgnore.Contains(parameter.Value))
                            {
                                continue;
                            }

                            thruster.Nickname = parameter.Value;
                            break;
                        case "ids_name":
                            thruster.Name = new InfocardDto
                                { Key = uint.Parse(parameter.Value) };
                            break;
                        case "ids_info":
                            thruster.Infocard = new InfocardDto
                                { Key = uint.Parse(parameter.Value) };
                            break;
                        case "hit_points":
                            thruster.HitPoints = int.Parse(parameter.Value);
                            break;
                        case "max_force":
                            thruster.MaxForce = int.Parse(parameter.Value);
                            break;
                        case "power_usage":
                            thruster.PowerUsage = float.Parse(parameter.Value);
                            break;
                    }

                    if (string.IsNullOrEmpty(thruster.Nickname) || string.IsNullOrWhiteSpace(thruster.Nickname))
                    {
                        continue;
                    }

                    _thrusters[thruster.Nickname] = thruster;
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(ParseStEquip)}] st_equip.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ParseStGood()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(ParseStGood)}] Parsing st_good.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.EQUIPMENT, "st_good.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                var parameters = section.GetParameters();

                if (parameters.Find(x => x.Key == "nickname")?.Value?.Contains("thruster") != true)
                {
                    continue;
                }

                var thruster = new ThrusterDto();
                
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_thrustersToIgnore.Contains(parameter.Value))
                            {
                                continue;
                            }

                            thruster.Nickname = parameter.Value;
                            break;
                        case "price":
                            thruster.BasePrice = float.Parse(parameter.Value);
                            break;
                    }
                    
                    if (string.IsNullOrEmpty(thruster.Nickname) || string.IsNullOrWhiteSpace(thruster.Nickname))
                    {
                        continue;
                    }
                    
                    if (_thrusters.TryGetValue(thruster.Nickname, out var existingThruster))
                    {
                        existingThruster.BasePrice = thruster.BasePrice;
                    }
                }
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(ParseStGood)}] st_good.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void AddInfocards()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(AddInfocards)}] Adding infocards to thrusters...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var thruster in _thrusters.Values)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(thruster.Name.Key);
                var infocard = _dataImporter.InfocardProcessor.GetInfocard(thruster.Infocard.Key);
                
                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(ThrusterProcessor)}] [{nameof(AddInfocards)}] Name not found for thruster: {thruster.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }
                
                if (infocard is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(ThrusterProcessor)}] [{nameof(AddInfocards)}] Infocard not found for thruster: {thruster.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }
                
                thruster.Name = name;
                thruster.Infocard = infocard;
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(AddInfocards)}] Infocards added to thrusters successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void CreateOrUpdateScriptableObjects()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Creating thruster scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var thruster in _thrusters.Values)
            {
                string path = _dataImporter.GetScriptableObjectPath(thruster.Name.Value, FreelancerDataImporter.THRUSTER);
                
                if(FreelancerDataImporter.TryGetScriptableObject(path, out ThrusterSO thrusterSO))
                {
                    thrusterSO.Initialize(thruster);
                    continue;
                }
                
                thrusterSO = ScriptableObject.CreateInstance<ThrusterSO>();
                thrusterSO.Initialize(thruster);
                
                _dataImporter.CreateSO(thrusterSO, path);
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Thruster scriptable objects created successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        public UniTask Load()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(Load)}] Loading thrusters...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            ParseStEquip();
            ParseStGood();

            AddInfocards();

            CreateOrUpdateScriptableObjects();

            _dataImporter.Logger.LogInfo($"[{nameof(ThrusterProcessor)}] [{nameof(Load)}] Thrusters loaded successfully", FreelancerDataTool.FREELANCER_DATA_TOOL);
            return UniTask.CompletedTask;
        }
    }
}