using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cysharp.Threading.Tasks;
using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Models;
using FreelancerDataTool.ScriptableObjects;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

namespace FreelancerDataTool
{
    public class FactionProcessor
    {
        #region Factions To Ignore

        private readonly List<string> _factionsToIgnore = new()
        {
            "br_p_guardian",
            "fc_uk_grp",
            "pf_02_grp",
            "pf_03_grp",
            "pf_04_grp",
            "pf_05_grp",
            "pf_07_grp",
            "pf_08_grp",
            "pf_09_grp",
            "pf_10_grp",
            "pf_11_grp",
            "pf_12_grp",
            "pf_13_grp",
            "pf_14_grp",
            "pf_15_grp",
            "pf_16_grp",
            "pf_17_grp",
            "pf_18_grp",
            "pf_19_grp",
            "pf_20_grp",
            "pf_21_grp",
            "pf_22_grp",
            "pf_24_grp",
            "pf_25_grp",
            "pf_26_grp",
            "pf_27_grp",
            "pf_28_grp",
            "pf_29_grp",
            "pf_30_grp",
            "pf_32_grp",
            "pf_33_grp",
            "pf_34_grp",
            "pf_35_grp",
            "pf_36_grp",
            "pf_37_grp",
            "pf_38_grp",
            "pf_39_grp",
            "pf_40_grp",
            "pf_41_grp",
            "pf_42_grp",
            "pf_43_grp",
            "pf_44_grp",
            "pf_45_grp",
            "pf_46_grp",
            "pf_47_grp",
            "pf_48_grp",
            "pf_49_grp",
            "pf_50_grp"
        };

        #endregion

        private readonly FreelancerDataImporter _dataImporter;

        private readonly Dictionary<string, FactionDto> _factions = new();

        public Dictionary<string, IFaction> FactionsScriptableObjects { get; } = new();

        public FactionProcessor(FreelancerDataImporter dataImporter)
        {
            _dataImporter = dataImporter;
        }

        private void ParseInitialWorld()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(ParseInitialWorld)}] Parsing initialworld.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, "initialworld.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                if (section.Name.ToLower() != "group")
                {
                    continue;
                }

                var faction = new FactionDto();

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            if (_factionsToIgnore.Contains(parameter.Value))
                            {
                                break;
                            }
                            faction.Nickname = parameter.Value;
                            break;
                        case "ids_name":
                            faction.Name = new InfocardDto
                            {
                                Key = uint.Parse(parameter.Value)
                            };
                            break;
                        case "ids_info":
                            faction.Infocard = new InfocardDto
                            {
                                Key = uint.Parse(parameter.Value)
                            };
                            break;
                        case "ids_short_name":
                            faction.ShortName = new InfocardDto
                            {
                                Key = uint.Parse(parameter.Value)
                            };
                            break;
                        case "rep":
                            string[] split = parameter.Value.Replace(" ", "").Split(',');
                            if (_factionsToIgnore.Contains(split[1]))
                            {
                                break;
                            }

                            if (split[1] == faction.Nickname)
                            {
                                break;
                            }
                            faction.Rep[split[1]] = float.Parse(split[0]);
                            break;
                    }
                }

                if (string.IsNullOrEmpty(faction.Nickname) || string.IsNullOrWhiteSpace(faction.Nickname))
                {
                    continue;
                }

                _factions[faction.Nickname] = faction;
            }

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(ParseInitialWorld)}] initialworld.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ParseEmpathy()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(ParseEmpathy)}] Parsing empathy.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.MISSIONS, "empathy.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                FactionDto factionDto = null;

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "group":
                            if (_factionsToIgnore.Contains(parameter.Value))
                            {
                                break;
                            }

                            if (!_factions.ContainsKey(parameter.Value))
                            {
                                break;
                            }

                            factionDto = _factions[parameter.Value];
                            break;
                        case "event":
                            factionDto?.Event.Add(parameter.Value);
                            break;
                        case "empathy_rate":
                            factionDto?.EmpathyRate.Add(parameter.Value);
                            break;
                    }
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(ParseEmpathy)}] empathy.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ParseFactionProp()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(ParseFactionProp)}] Parsing faction_prop.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.MISSIONS, "faction_prop.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                FactionDto factionDto = null;

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "affiliation":
                            if (_factionsToIgnore.Contains(parameter.Value))
                            {
                                break;
                            }

                            if (!_factions.ContainsKey(parameter.Value))
                            {
                                break;
                            }

                            factionDto = _factions[parameter.Value];
                            break;
                        case "npc_ship":
                            factionDto?.NpcShips.Add(parameter.Value);
                            break;
                        case "legality":
                            if (factionDto == null)
                            {
                                break;
                            }

                            factionDto.Legality = parameter.Value;
                            break;
                        case "jump_preference":
                            if (factionDto == null)
                            {
                                break;
                            }

                            factionDto.JumpPreference = parameter.Value;
                            break;
                        case "scan_announce":
                            if (factionDto == null)
                            {
                                break;
                            }

                            factionDto.ScanAnnounce = bool.TryParse(parameter.Value, out bool scanAnnounce) && scanAnnounce;
                            break;
                        case "scan_chance":
                            if (factionDto == null)
                            {
                                break;
                            }

                            factionDto.ScanChance = float.Parse(parameter.Value);
                            break;
                    }
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(ParseFactionProp)}] faction_prop.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void CreateOrUpdateScriptableObjects()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Creating faction scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var faction) in _factions)
            {
                string path = _dataImporter.GetScriptableObjectPath(faction.Name.Value, FreelancerDataImporter.FACTION);

                if (FreelancerDataImporter.TryGetScriptableObject(path, out FactionSO factionSO))
                {
                    factionSO.Initialize(faction);

                    FactionsScriptableObjects[faction.Nickname] = factionSO;
                    continue;
                }

                factionSO = ScriptableObject.CreateInstance<FactionSO>();
                factionSO.Initialize(faction);

                FactionsScriptableObjects[faction.Nickname] = factionSO;

                _dataImporter.CreateSO(factionSO, path);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Faction scriptable objects created successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void AddInfocardsAndLogos()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(AddInfocardsAndLogos)}] Adding infocards and logos...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var faction) in _factions)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(faction.Name.Key);
                var shortName = _dataImporter.InfocardProcessor.GetInfocard(faction.ShortName.Key);
                var infocardInfocard = _dataImporter.InfocardProcessor.GetInfocard(faction.Infocard.Key);

                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddInfocardsAndLogos)}] Faction name infocard not found for {faction.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);

                    continue;
                }

                if (shortName is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddInfocardsAndLogos)}] Faction short name infocard not found for {faction.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                }

                if (infocardInfocard is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddInfocardsAndLogos)}] Faction infocard not found for {faction.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                }

                faction.Name = name;
                faction.ShortName = shortName;
                faction.Infocard = infocardInfocard;

                if (TryGetFactionLogo(faction.Name.Value, out var factionLogo))
                {
                    faction.Logo = factionLogo;
                }
                else
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddInfocardsAndLogos)}] Faction logo not found for {faction.Name.Value}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                }
            }

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(AddInfocardsAndLogos)}] Added infocards and logos", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void AddReputations()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(AddReputations)}] Adding reputations...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            if (AssetDatabase.FindAssets("SO_Faction_").Length == 0)
            {
                _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddReputations)}] Faction scriptable objects not found. Skipping adding reputations.", FreelancerDataTool.FREELANCER_DATA_TOOL);
                return;
            }

            foreach ((string _, var faction) in _factions)
            {
                foreach ((string factionRepInternal, float factionRepValue) in faction.Rep)
                {
                    if (factionRepValue == 0)
                    {
                        continue;
                    }

                    if (!_factions.TryGetValue(factionRepInternal, out var factionRep))
                    {
                        _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddReputations)}] Faction reputation not found for {factionRepInternal}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                        continue;
                    }

                    string factionRepName = factionRep.Name.Value;
                    if (!FreelancerDataImporter.TryGetScriptableObject(_dataImporter.GetScriptableObjectPath(factionRepName, FreelancerDataImporter.FACTION), out FactionSO factionRepSO))
                    {
                        _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddReputations)}] Faction scriptable object not found for {factionRepName}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                        continue;
                    }

                    faction.Reputations[factionRepSO] = factionRepValue;
                }

                var orderedReputations = faction.Reputations.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
                faction.Reputations = orderedReputations;

                var factionSO = FactionsScriptableObjects[faction.Nickname];
                factionSO.Reputations.Clear();
                factionSO.Reputations.AddRange(orderedReputations);
                
                EditorUtility.SetDirty((FactionSO)factionSO);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(AddReputations)}] Added reputations", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void AddShipsUsed()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(AddShipsUsed)}] Adding ships used...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach (var factionDto in _factions.Values)
            {
                var factionSO = FactionsScriptableObjects[factionDto.Nickname];
                factionSO.ShipsUsed.Clear();

                foreach (string npcShip in factionDto.NpcShips)
                {
                    if (!_dataImporter.ShipProcessor.NpcShipsScriptableObjects.TryGetValue(npcShip, out var ship))
                    {
                        _dataImporter.Logger.LogWarning($"[{nameof(FactionProcessor)}] [{nameof(AddShipsUsed)}] Ship not found for {npcShip}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                        continue;
                    }

                    if (factionSO.ShipsUsed.Contains(ship))
                    {
                        continue;
                    }

                    factionSO.ShipsUsed.Add(ship);
                }

                var orderedShipsUsed = factionSO.ShipsUsed.OrderBy(x => (int)x.ShipClass).ToList();

                factionSO.ShipsUsed.Clear();
                factionSO.ShipsUsed.AddRange(orderedShipsUsed);
                
                EditorUtility.SetDirty((FactionSO)factionSO);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(AddShipsUsed)}] Added ships used", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }


        private static bool TryGetFactionLogo(string factionName, out Sprite factionLogo)
        {
            string[] factionLogoGuid = AssetDatabase.FindAssets($"TEX_Factions_{factionName.Replace(" ", "")}");
            if (factionLogoGuid.Length <= 0)
            {
                factionLogo = null;
                return false;
            }

            string factionLogoPath = AssetDatabase.GUIDToAssetPath(factionLogoGuid[0]);
            factionLogo = AssetDatabase.LoadAssetAtPath<Sprite>(factionLogoPath);

            return factionLogo is not null;
        }

        public UniTask Load()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(Load)}] Loading factions...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            ParseInitialWorld();
            ParseEmpathy();
            ParseFactionProp();

            AddInfocardsAndLogos();

            CreateOrUpdateScriptableObjects();

            _dataImporter.Logger.LogInfo($"[{nameof(FactionProcessor)}] [{nameof(Load)}] Factions loaded successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);

            return UniTask.CompletedTask;
        }

        public void AddAdditionalData()
        {
            AddReputations();
            AddShipsUsed();
        }
    }
}