using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;

namespace FreelancerDataTool.Models
{
    public class CounterMeasureDto : ICounterMeasure
    {
        public string Nickname { get; set;}
        public IInfocard Name { get; set;}
        public IInfocard Infocard { get; set;}
        public float Volume { get; set;}
        public decimal PriceModifier { get; set;}
        public float BasePrice { get; set;}
        public GoodType GoodType { get; set;}
        public int HitPoints { get; set;}
        public float ExplosionResistance { get; set;}
        public int Mass { get; set;}
        public float PowerUsage { get; set;}
        public float RefireDelay { get; set;}
        public int MuzzleVelocity { get; set;}
        public ICounterMeasureAmmo Ammo { get; set;}
    }
}