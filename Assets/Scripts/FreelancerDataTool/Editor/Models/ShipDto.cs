using System;
using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;

namespace FreelancerDataTool.Models
{
    public class ShipDto : IShip
    {
        public string Nickname { get; set; }
        public IInfocard Name { get; set; }
        public IInfocard Infocard { get; set; }
        public float CargoSize { get; set; }
        public float LinearDrag { get; set; }
        public float NudgeForce { get; set; }
        public Tuple<float, float, float> SteeringTorque { get; set; }
        public Tuple<float, float, float> AngularDrag { get; set; }
        public Tuple<float, float, float> RotationInertia { get; set; }
        public int Mass { get; set; }
        public int StrafeForce { get; set; }
        public int StrafePowerUsage { get; set; }
        public int MaxBankAngle { get; set; }
        public int HoldSize { get; set; }
        public int HitPoints { get; set; }
        public int Nanobots { get; set; }
        public int ShieldBats { get; set; }
        public ShipClass ShipClass { get; set; }
        public ShipType ShipType { get; set; }
        public int StockA { get; set; }
        public int StockB { get; set; }
        public float Volume { get; set; }
        public decimal PriceModifier { get; set; }
        public bool BaseSells { get; set; }
        public float BasePrice { get; set; }
        public GoodType GoodType { get; set; }
    }
}