using System.Collections.Generic;
using FreelancerDataTool.Interfaces;

namespace FreelancerDataTool.Models
{
    public class NpcDto : INpc
    {
        public string Nickname { get; set; }
        public IInfocard Name { get; set; }
        public IInfocard Infocard { get; set; }
        public string Room { get; set; }
        public IBase Base { get; set; }
        public IFaction Affiliation { get; set; }
        public Dictionary<IFaction, int> Bribes { get; } = new();
        public List<IInfocard> Rumors { get; set; } = new();
        public string BaseNickname { get; set; }
        public string AffiliationNickname { get; set; }
        public Dictionary<string, int> BribesNicknames { get; set; } = new();
    }
}