using System.Collections.Generic;
using FreelancerDataTool.Interfaces;
using UnityEngine;

namespace FreelancerDataTool.Models
{
    public class FactionDto : IFaction
    {
        public string Nickname { get; set; }
        public IInfocard Name { get; set; }
        public IInfocard ShortName { get; set; }
        public IInfocard Infocard { get; set; }
        public string Legality { get; set; }
        public string JumpPreference { get; set; }
        public Sprite Logo { get; set; }
        public Dictionary<IFaction, float> Reputations { get; set; } = new();
        public Dictionary<INpc, float> NpcBribers { get; } = new();
        public List<IShip> ShipsUsed { get; } = new();
        public List<IBase> OwnedBases { get; } = new();
        public List<INpc> Npcs { get; } = new();
        public float ScanChance { get; set; }
        public bool ScanAnnounce { get; set; }
        public List<string> Event { get; } = new();
        public List<string> EmpathyRate { get; } = new();
        public List<string> NpcShips { get; } = new();
        public Dictionary<string, float> Rep { get; } = new();
    }
}