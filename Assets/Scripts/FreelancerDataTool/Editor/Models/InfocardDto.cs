using FreelancerDataTool.Interfaces;

namespace FreelancerDataTool.Models
{
    public class InfocardDto : IInfocard
    {
        public string Value { get; set; }
        public uint Key { get; set; }
    }
}