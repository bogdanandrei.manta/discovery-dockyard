using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;

namespace FreelancerDataTool.Models
{
    public class ShieldDto : IShield
    {
        public string Nickname { get; set; }
        public IInfocard Name { get; set; }
        public IInfocard Infocard { get; set; }
        public float Volume { get; set; }
        public decimal PriceModifier { get; set; }
        public float BasePrice { get; set; }
        public GoodType GoodType { get; set; }
        public int HitPoints { get; set; }
        public float ExplosionResistance { get; set; }
        public float RegenerationRate { get; set; }
        public float MaxCapacity { get; set; }
        public float OfflineRebuildTime { get; set; }
        public float OfflineThreshold { get; set; }
        public int ConstantPowerDraw { get; set; }
        public int RebuildPowerDraw { get; set; }
    }
}