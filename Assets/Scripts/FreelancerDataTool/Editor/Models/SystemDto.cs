using System;
using System.Collections.Generic;
using FreelancerDataTool.Interfaces;

namespace FreelancerDataTool.Models
{
    public class SystemDto : ISystem
    {
        public string Nickname { get; set; }
        public IInfocard Name { get; set; }
        public IInfocard Infocard { get; set; }
        public string FilePath { get; set; }
        public string Region { get; set; }
        public Tuple<int, int> Position { get; set; }
        public List<IBase> Bases { get; set; }
        public float NavMapScale { get; set; }
    }
}