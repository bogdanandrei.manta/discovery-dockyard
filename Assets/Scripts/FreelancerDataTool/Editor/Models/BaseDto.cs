using System;
using System.Collections.Generic;
using FreelancerDataTool.Interfaces;

namespace FreelancerDataTool.Models
{
    public class BaseDto : IBase
    {
        public string Nickname { get; set; }
        public string Base { get; set; }
        public IInfocard Name { get; set; }
        public IInfocard Infocard { get; set; }
        public ISystem System { get; set; }
        public IFaction Reputation { get; set; }
        public string FilePath { get; set; }
        public Tuple<float, float, float> Position { get; set; }
        public Tuple<float, float, float> Rotation { get; set; }
        public string Archetype { get; set;  }
        public string ReputationNickname { get; set; }
        public string SystemNickname { get; set; }
        public List<string> InhabitingFactionsNicknames { get; set; } = new();
        public List<string> NpcNicknames { get; set; } = new();
    }
}