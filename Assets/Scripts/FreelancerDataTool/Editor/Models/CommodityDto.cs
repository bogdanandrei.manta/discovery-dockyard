using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;

namespace FreelancerDataTool.Models
{
    public class CommodityDto : IGood
    {
        public string Nickname { get; set; }
        public IInfocard Name { get; set; }
        public IInfocard Infocard { get; set; }
        public int StockA { get; set; }
        public int StockB { get; set; }
        public float Volume { get; set; }
        public decimal PriceModifier { get; set; }
        public bool BaseSells { get; set; }
        public float BasePrice { get; set; }
        public GoodType GoodType { get; set; }
    }
}