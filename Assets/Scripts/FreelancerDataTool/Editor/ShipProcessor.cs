using System;
using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Models;
using FreelancerDataTool.ScriptableObjects;
using FreelancerDataTool.Types;
using UnityEngine;

namespace FreelancerDataTool
{
    public class ShipProcessor
    {
        private readonly FreelancerDataImporter _dataImporter;

        private readonly List<uint> _shipsToIgnore = new()
        {
            55022,
            261164
        };

        private readonly Dictionary<string, ShipDto> _ships = new();

        public Dictionary<string, IShip> ShipsScriptableObjects { get; } = new();
        public Dictionary<string, IShip> NpcShipsScriptableObjects { get; } = new();

        public ShipProcessor(FreelancerDataImporter dataImporter)
        {
            _dataImporter = dataImporter;
        }

        private void ParseShipArch()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(ParseShipArch)}] Parsing shiparch.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.SHIPS, "shiparch.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                if (section.Name.ToLower() != "ship")
                {
                    continue;
                }

                var ship = new ShipDto();

                var parameters = section.GetParameters();
                foreach (var parameter in parameters)
                {
                    switch (parameter.Key)
                    {
                        case "nickname":
                            ship.Nickname = parameter.Value;
                            break;
                        case "ids_name":
                            ship.Name = new InfocardDto
                            {
                                Key = uint.Parse(parameter.Value)
                            };
                            break;
                        case "ids_info":
                            ship.Infocard = new InfocardDto
                            {
                                Key = uint.Parse(parameter.Value)
                            };
                            break;
                        case "nanobot_limit":
                            ship.Nanobots = int.Parse(parameter.Value);
                            break;
                        case "shield_battery_limit":
                            ship.ShieldBats = int.Parse(parameter.Value);
                            break;
                        case "type":
                            ship.ShipType = Enum.TryParse(parameter.Value, true, out ShipType type) ? type : ShipType.UNKNOWN;
                            break;
                        case "ship_class":
                            ship.ShipClass = Enum.TryParse(parameter.Value, true, out ShipClass shipClass) ? shipClass : ShipClass.UNKNOWN;
                            break;
                        case "mass":
                            ship.Mass = (int)float.Parse(parameter.Value);
                            break;
                        case "hold_size":
                            ship.HoldSize = int.Parse(parameter.Value);
                            break;
                        case "linear_drag":
                            ship.LinearDrag = float.Parse(parameter.Value);
                            break;
                        case "max_bank_angle":
                            ship.MaxBankAngle = (int)float.Parse(parameter.Value);
                            break;
                        case "hit_pts":
                            ship.HitPoints = int.Parse(parameter.Value);
                            break;
                        case "steering_torque":
                            string[] steeringTorque = parameter.Value.Replace(" ", "").Split(',');
                            ship.SteeringTorque = new Tuple<float, float, float>(
                                float.Parse(steeringTorque[0]),
                                float.Parse(steeringTorque[1]),
                                float.Parse(steeringTorque[2])
                            );
                            break;
                        case "angular_drag":
                            string[] angularDrag = parameter.Value.Replace(" ", "").Split(',');
                            ship.AngularDrag = new Tuple<float, float, float>(
                                float.Parse(angularDrag[0]),
                                float.Parse(angularDrag[1]),
                                float.Parse(angularDrag[2])
                            );
                            break;
                        case "rotation_inertia":
                            string[] rotationInertia = parameter.Value.Replace(" ", "").Split(',');
                            ship.RotationInertia = new Tuple<float, float, float>(
                                float.Parse(rotationInertia[0]),
                                float.Parse(rotationInertia[1]),
                                float.Parse(rotationInertia[2])
                            );
                            break;
                        case "nudge_force":
                            ship.NudgeForce = float.Parse(parameter.Value);
                            break;
                        case "strafe_force":
                            ship.StrafeForce = (int)float.Parse(parameter.Value);
                            break;
                        case "strafe_power_usage":
                            ship.StrafePowerUsage = int.Parse(parameter.Value);
                            break;
                    }
                }

                if (_shipsToIgnore.Contains(ship.Name.Key))
                {
                    continue;
                }

                _ships[ship.Nickname] = ship;
            }

            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(ParseShipArch)}] shiparch.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void AddInfocards()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(AddInfocards)}] Adding infocards to ships...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var ship) in _ships)
            {
                var name = _dataImporter.InfocardProcessor.GetInfocard(ship.Name.Key);
                var infocard = _dataImporter.InfocardProcessor.GetInfocard(ship.Infocard.Key);

                if (name is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(ShipProcessor)}] [{nameof(AddInfocards)}] Name not found for ship: {ship.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                if (infocard is null)
                {
                    _dataImporter.Logger.LogWarning($"[{nameof(ShipProcessor)}] [{nameof(AddInfocards)}] Infocard not found for ship: {ship.Nickname}", FreelancerDataTool.FREELANCER_DATA_TOOL);
                    continue;
                }

                ship.Name = name;
                ship.Infocard = infocard;
            }

            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(AddInfocards)}] Infocards added to ships successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void CreateOrUpdateScriptableObjects()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Creating ship scriptable objects...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            foreach ((string _, var ship) in _ships)
            {
                string path = _dataImporter.GetScriptableObjectPath(ship.Name.Value, FreelancerDataImporter.SHIP);

                if (FreelancerDataImporter.TryGetScriptableObject(path, out ShipSO shipSO))
                {
                    shipSO.Initialize(ship);

                    ShipsScriptableObjects[ship.Nickname] = shipSO;
                    continue;
                }

                shipSO = ScriptableObject.CreateInstance<ShipSO>();
                shipSO.Initialize(ship);

                ShipsScriptableObjects[ship.Nickname] = shipSO;

                _dataImporter.CreateSO(shipSO, path);
            }

            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(CreateOrUpdateScriptableObjects)}] Ship scriptable objects created successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        private void ParseNpcShips()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(ParseNpcShips)}] Parsing npcships.ini...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            string iniPath = Path.Combine(_dataImporter.FreelancerPath, FreelancerDataImporter.DATA, FreelancerDataImporter.MISSIONS, "npcships.ini");

            var ini = _dataImporter.ReadIniFile(iniPath);

            if (ini is null)
            {
                return;
            }

            var sections = ini.GetSections();
            foreach (var section in sections)
            {
                var parameters = section.GetParameters();

                var nickname = parameters.Find(x => x.Key == "nickname")?.Value;

                if (string.IsNullOrEmpty(nickname))
                {
                    continue;
                }

                var shipArchetype = parameters.Find(x => x.Key == "ship_archetype")?.Value;

                if (string.IsNullOrEmpty(shipArchetype))
                {
                    continue;
                }

                if (!ShipsScriptableObjects.TryGetValue(shipArchetype, out var ship))
                {
                    continue;
                }

                NpcShipsScriptableObjects[nickname] = ship;
            }
            
            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(ParseNpcShips)}] npcships.ini parsed successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
        }

        public UniTask Load()
        {
            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(Load)}] Loading ships...", FreelancerDataTool.FREELANCER_DATA_TOOL);

            ParseShipArch();

            AddInfocards();

            CreateOrUpdateScriptableObjects();

            ParseNpcShips();

            _dataImporter.Logger.LogInfo($"[{nameof(ShipProcessor)}] [{nameof(Load)}] Ships loaded successfully.", FreelancerDataTool.FREELANCER_DATA_TOOL);
            return UniTask.CompletedTask;
        }
    }
}