using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_Infocard_", menuName = "Scriptable Objects/New Infocard")]
    [InlineEditor(InlineEditorModes.GUIAndPreview, InlineEditorObjectFieldModes.Foldout, DrawPreview = true)]
    public class InfocardSO : ScriptableObject, IInfocard
    {
        [field: DarkBox]
        [field: LabelWidth(100)]
        [field: SerializeField]
        public uint Key { get; private set; }

        [field: DarkBox]
        [field: HideLabel]
        [field: TextArea(5, 30)]
        [field: SerializeField]
        public string Value { get; private set; }

#if UNITY_EDITOR
        public void Initialize(IInfocard infocard)
        {
            Key = infocard.Key;
            Value = infocard.Value;
        }
#endif
    }
}