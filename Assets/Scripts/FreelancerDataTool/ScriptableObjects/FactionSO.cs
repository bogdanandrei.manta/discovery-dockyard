using System.Collections.Generic;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_Faction_", menuName = "Scriptable Objects/New Faction")]
    [InlineEditor(InlineEditorModes.GUIAndPreview, InlineEditorObjectFieldModes.Foldout, DrawPreview = true)]
    public class FactionSO : SerializedScriptableObject, IFaction
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }

        [field: Title("@ShortName != null ? ShortName.Value : \"Short Name\"", "@ShortName != null ? ShortName.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard ShortName { get; private set; }

        [field: Title("Infocard", "@Infocard != null ? Infocard.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IInfocard Infocard { get; private set; }

        [field: Title("Logo")]
        [field: VerticalGroup("Split/Right")]
        [field: PreviewField(100, ObjectFieldAlignment.Center)]
        [field: HideLabel]
        [field: SerializeField]
        public Sprite Logo { get; private set; }

        [field: Title("Reputations")]
        [field: HideLabel]
        [field: OdinSerialize]
        public Dictionary<IFaction, float> Reputations { get; private set; }

        [field: Title("NPC Bribers")]
        [field: HideLabel]
        [field: OdinSerialize]
        public Dictionary<INpc, float> NpcBribers { get; private set; }

        [field: Title("Ships Used")]
        [field: HideLabel]
        [field: OdinSerialize]
        public List<IShip> ShipsUsed { get; private set; }

        [field: Title("Owned Bases")]
        [field: HideLabel]
        [field: OdinSerialize]
        public List<IBase> OwnedBases { get; private set; }

        [field: Title("NPCs")]
        [field: HideLabel]
        [field: OdinSerialize]
        public List<INpc> Npcs { get; private set; }

        [field: Title("Extra Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public string Legality { get; set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public string JumpPreference { get; set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public bool ScanAnnounce { get; set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public float ScanChance { get; set; }


#if UNITY_EDITOR
        public void Initialize(IFaction faction)
        {
            Nickname = faction.Nickname;
            Name = faction.Name;
            ShortName = faction.ShortName;
            Infocard = faction.Infocard;
            Logo = faction.Logo;
            Reputations = faction.Reputations;
            NpcBribers = faction.NpcBribers;
            ShipsUsed = faction.ShipsUsed;
            OwnedBases = faction.OwnedBases;
            Npcs = faction.Npcs;
            Legality = faction.Legality;
            JumpPreference = faction.JumpPreference;
            ScanAnnounce = faction.ScanAnnounce;
            ScanChance = faction.ScanChance;
        }
#endif
    }
}