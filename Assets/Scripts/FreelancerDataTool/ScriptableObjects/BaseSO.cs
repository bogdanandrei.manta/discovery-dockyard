using System;
using FreelancerDataTool.Interfaces;
using JetBrains.Annotations;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_Base_", menuName = "Scriptable Objects/New Base")]
    public class BaseSO : SerializedScriptableObject, IBase, IInitializable<IBase>
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }

        [field: Title("Infocard", "@Infocard != null ? Infocard.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IInfocard Infocard { get; private set; }

        public Tuple<float, float, float> Position { get; private set; }
        public Tuple<float, float, float> Rotation { get; private set; }
        public string Archetype { get; private set; }
        
        [field: Title("@System != null ? SystemName : \"System\"", "@System != null ? SystemNameKey : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public ISystem System { get; private set; }
        
        [field: Title("@Reputation != null ? ReputationName : \"Reputation\"", "@Reputation != null ? ReputationNameKey : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IFaction Reputation { get; private set; }
        public string FilePath { get; private set; }
        
#if UNITY_EDITOR

        [UsedImplicitly]
        private string SystemName => System?.Name?.Value;
        [UsedImplicitly]
        private string SystemNameKey => System?.Name?.Key.ToString();
        [UsedImplicitly]
        private string ReputationName => Reputation?.Name?.Value;
        [UsedImplicitly]
        private string ReputationNameKey => Reputation?.Name?.Key.ToString();
#endif

        public void Initialize(IBase baseObject)
        {
            Nickname = baseObject.Nickname;
            Name = baseObject.Name;
            Infocard = baseObject.Infocard;
            Position = baseObject.Position;
            Rotation = baseObject.Rotation;
            Archetype = baseObject.Archetype;
            System = baseObject.System;
            Reputation = baseObject.Reputation;
            FilePath = baseObject.FilePath;
        }
    }
}