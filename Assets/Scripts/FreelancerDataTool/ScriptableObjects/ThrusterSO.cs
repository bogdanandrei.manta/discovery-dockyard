using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_Thruster_", menuName = "Scriptable Objects/New Thruster")]
    public class ThrusterSO : SerializedScriptableObject, IThruster
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }

        [field: Title("Infocard", "@Infocard != null ? Infocard.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IInfocard Infocard { get; private set; }

        public float Volume { get; private set; }
        public decimal PriceModifier { get; private set; }
        public float BasePrice { get; private set; }
        public GoodType GoodType { get; private set; }
        public int HitPoints { get; private set; }
        public float ExplosionResistance { get; }
        public int Mass { get; private set; }
        public float PowerUsage { get; private set; }
        public int MaxForce { get; private set; }

#if UNITY_EDITOR
        public void Initialize(IThruster thruster)
        {
            Nickname = thruster.Nickname;
            Name = thruster.Name;
            Infocard = thruster.Infocard;
            Volume = thruster.Volume;
            PriceModifier = thruster.PriceModifier;
            BasePrice = thruster.BasePrice;
            GoodType = thruster.GoodType;
            HitPoints = thruster.HitPoints;
            Mass = thruster.Mass;
            PowerUsage = thruster.PowerUsage;
            MaxForce = thruster.MaxForce;
        }
  #endif
    }
}