using System.Collections.Generic;
using FreelancerDataTool.Interfaces;
using JetBrains.Annotations;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    public class NpcSO : SerializedScriptableObject, INpc, IInitializable<INpc>
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value.Replace(\"\n\", \" \") : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: PropertySpace(10)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }
        
        public IInfocard Infocard { get; private set; }
        
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public string Room { get; private set; }

        [field: Title("@Base != null ? BaseName : \"Base\"", "@Base != null ? BaseNameId : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IBase Base { get; private set; }

        [field: Title("@Affiliation != null ? AffiliationName : \"Affiliation\"", "@Affiliation != null ? AffiliationNameId : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IFaction Affiliation { get; private set;}
        
        [field: Title("Bribes")]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public Dictionary<IFaction, int> Bribes { get; private set; }
        
        [field: Title("Rumors")]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public List<IInfocard> Rumors { get; private set; }

#if UNITY_EDITOR
        [UsedImplicitly]
        private string AffiliationName => Affiliation?.Name?.Value;
        
        [UsedImplicitly]
        private string AffiliationNameId => Affiliation?.Name?.Key.ToString();
        
        [UsedImplicitly]
        private string BaseName => Base?.Name?.Value;
        
        [UsedImplicitly]
        private string BaseNameId => Base?.Name?.Key.ToString();
#endif
        
        public void Initialize(INpc data)
        {
            Nickname = data.Nickname;
            Name = data.Name;
            Infocard = data.Infocard;
            Room = data.Room;
            Base = data.Base;
            Affiliation = data.Affiliation;
            Bribes = data.Bribes;
            Rumors = data.Rumors;
        }
    }
}