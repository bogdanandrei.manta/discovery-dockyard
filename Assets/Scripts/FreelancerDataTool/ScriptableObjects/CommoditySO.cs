using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_Commodity_", menuName = "Scriptable Objects/New Commodity")]
    public class CommoditySO : SerializedScriptableObject, IGood
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }

        [field: Title("Infocard", "@Infocard != null ? Infocard.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IInfocard Infocard { get; private set; }
        public int StockA { get; private set;}
        public int StockB { get; private set;}
        public float Volume { get; private set;}
        public decimal PriceModifier { get; private set;}
        public bool BaseSells { get; private set;}
        public float BasePrice { get; private set;}
        public GoodType GoodType { get; private set;}

#if UNITY_EDITOR
        public void Initialize(IGood good)
        {
            Nickname = good.Nickname;
            Name = good.Name;
            Infocard = good.Infocard;
            Volume = good.Volume;
            PriceModifier = good.PriceModifier;
            BasePrice = good.BasePrice;
            GoodType = good.GoodType;
        }
#endif
    }
}