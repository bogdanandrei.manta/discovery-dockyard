using System;
using System.Collections.Generic;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_System_", menuName = "Scriptable Objects/New System")]
    public class SystemSO : SerializedScriptableObject, ISystem
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }

        [field: Title("Infocard", "@Infocard != null ? Infocard.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IInfocard Infocard { get; private set; }
        public string FilePath { get; private set; }
        public string Region { get; private set; }
        public Tuple<int, int> Position { get; private set; }
        public List<IBase> Bases { get; private set; }
        public float NavMapScale { get; private set; }
        
#if UNITY_EDITOR
        public void Initialize(ISystem system)
        {
            Nickname = system.Nickname;
            Name = system.Name;
            Infocard = system.Infocard;
            FilePath = system.FilePath;
            Position = system.Position;
            Bases = system.Bases;
            NavMapScale = system.NavMapScale;
        }
#endif
    }
}