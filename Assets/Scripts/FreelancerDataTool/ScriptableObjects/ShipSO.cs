using System;
using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_Ship_", menuName = "Scriptable Objects/New Ship")]
    public class ShipSO : SerializedScriptableObject, IShip
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }

        [field: Title("Infocard", "@Infocard != null ? Infocard.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IInfocard Infocard { get; private set; }

        [field: VerticalGroup("Split/Right")]
        [field: PreviewField(100, ObjectFieldAlignment.Center)]
        [field: HideLabel]
        [field: SerializeField]
        public GameObject Model { get; private set; }

        [field: Title("Ships Stats")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int HoldSize { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int HitPoints { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int Nanobots { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int ShieldBats { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public ShipClass ShipClass { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public ShipType ShipType { get; private set; }

        [field: Title("Extra Stats")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int Mass { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int StrafeForce { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int StrafePowerUsage { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public int MaxBankAngle { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public float LinearDrag { get; private set; }

        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: SerializeField]
        public float NudgeForce { get; private set; }

        [field: HideReferenceObjectPicker]
        [field: DarkBox]
        [field: LabelWidth(100)]
        [field: InlineProperty]
        [field: OdinSerialize]
        public Tuple<float, float, float> SteeringTorque { get; private set; }

        [field: HideReferenceObjectPicker]
        [field: DarkBox]
        [field: LabelWidth(100)]
        [field: InlineProperty]
        [field: OdinSerialize]
        public Tuple<float, float, float> AngularDrag { get; private set; }

        [field: HideReferenceObjectPicker]
        [field: DarkBox]
        [field: LabelWidth(100)]
        [field: InlineProperty]
        [field: OdinSerialize]
        public Tuple<float, float, float> RotationInertia { get; private set; }

        public int StockA { get; private set; }
        public int StockB { get; private set; }
        public float Volume { get; private set; }
        public decimal PriceModifier { get; private set; }
        public bool BaseSells { get; private set; }
        public float BasePrice { get; private set; }
        public GoodType GoodType { get; private set; }

#if UNITY_EDITOR
        public void Initialize(IShip ship)
        {
            Nickname = ship.Nickname;
            Name = ship.Name;
            Infocard = ship.Infocard;
            // Model = ship.Model;
            HoldSize = ship.HoldSize;
            HitPoints = ship.HitPoints;
            Nanobots = ship.Nanobots;
            ShieldBats = ship.ShieldBats;
            ShipClass = ship.ShipClass;
            ShipType = ship.ShipType;
            Mass = ship.Mass;
            StrafeForce = ship.StrafeForce;
            StrafePowerUsage = ship.StrafePowerUsage;
            MaxBankAngle = ship.MaxBankAngle;
            LinearDrag = ship.LinearDrag;
            NudgeForce = ship.NudgeForce;
            SteeringTorque = ship.SteeringTorque;
            AngularDrag = ship.AngularDrag;
            RotationInertia = ship.RotationInertia;
            PriceModifier = ship.PriceModifier;
            BasePrice = ship.BasePrice;
            GoodType = ship.GoodType;
        }
#endif
    }
}