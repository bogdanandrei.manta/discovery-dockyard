using FreelancerDataTool.Interfaces;
using FreelancerDataTool.Types;
using OdinInspector;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace FreelancerDataTool.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SO_Shield_", menuName = "Scriptable Objects/New Shield")]
    public class ShieldSO : SerializedScriptableObject, IShield
    {
        [field: Title("General Info")]
        [field: LabelWidth(100)]
        [field: DarkBox]
        [field: HorizontalGroup("Split", Width = .75f)]
        [field: VerticalGroup("Split/Left")]
        [field: SerializeField]
        public string Nickname { get; private set; }

        [field: Title("@Name != null ? Name.Value : \"Name\"", "@Name != null ? Name.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: HideLabel]
        [field: DarkBox]
        [field: VerticalGroup("Split/Left")]
        [field: OdinSerialize]
        public IInfocard Name { get; private set; }

        [field: Title("Infocard", "@Infocard != null ? Infocard.Key.ToString() : \"ID\"", TitleAlignments.Split)]
        [field: DarkBox]
        [field: HideLabel]
        [field: OdinSerialize]
        public IInfocard Infocard { get; private set; }

        public float Volume { get; private set; }
        public decimal PriceModifier { get; private set; }
        public float BasePrice { get; private set; }
        public GoodType GoodType { get; private set; }
        public int HitPoints { get; private set; }
        public float ExplosionResistance { get; private set; }
        public float RegenerationRate { get; private set; }
        public float MaxCapacity { get; private set; }
        public float OfflineRebuildTime { get; private set; }
        public float OfflineThreshold { get; private set; }
        public int ConstantPowerDraw { get; private set; }
        public int RebuildPowerDraw { get; private set; }

        #if UNITY_EDITOR
        public void Initialize(IShield shield)
        {
            Nickname = shield.Nickname;
            Name = shield.Name;
            Infocard = shield.Infocard;
            Volume = shield.Volume;
            PriceModifier = shield.PriceModifier;
            BasePrice = shield.BasePrice;
            GoodType = shield.GoodType;
            HitPoints = shield.HitPoints;
            ExplosionResistance = shield.ExplosionResistance;
            RegenerationRate = shield.RegenerationRate;
            MaxCapacity = shield.MaxCapacity;
            OfflineRebuildTime = shield.OfflineRebuildTime;
            OfflineThreshold = shield.OfflineThreshold;
            ConstantPowerDraw = shield.ConstantPowerDraw;
            RebuildPowerDraw = shield.RebuildPowerDraw;
        }
  #endif
    }
}