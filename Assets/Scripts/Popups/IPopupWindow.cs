namespace Popups
{
    public interface IPopupWindow<in T>
    {
        void SetupPopup(T data, PopupUiController popupUiController);
        void ClosePopup();
    }
}