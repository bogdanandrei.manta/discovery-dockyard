using DependencyInjection;
using FreelancerDataTool.Interfaces;

namespace Popups
{
    public interface IPopupService : IService
    {
        void SetPopupUiController(PopupUiController popupUiController);
        void ShowPopup(IFaction faction);
    }
}