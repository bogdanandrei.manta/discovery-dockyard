using DependencyInjection;
using FreelancerDataTool.Interfaces;

namespace Popups
{
    public class PopupService : IPopupService
    {
        private PopupUiController _popupUiController;
        
        public void Initialize(DependencyContainer container)
        {
            
        }
        
        public void SetPopupUiController(PopupUiController popupUiController)
        {
            _popupUiController = popupUiController;
        }

        public void ShowPopup(IFaction faction)
        {
            _popupUiController.ShowPopup(faction);
        }
    }
}