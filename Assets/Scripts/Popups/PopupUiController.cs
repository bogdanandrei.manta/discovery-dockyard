using System.Collections.Generic;
using DependencyInjection;
using Factions;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Popups
{
    public class PopupUiController : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _popupContainer;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private GameObject _popupBackground;

        [Required]
        [AssetList]
        [SerializeField]
        private FactionPopupWindow _factionPopupWindowPrefab;

        private IPopupService _popupService;

        private readonly Stack<GameObject> _popupStack = new();

        private void Awake()
        {
            _popupBackground.SetActive(false);

            _popupService = DependencyContainer.Instance.Resolve<IPopupService>();
            _popupService.SetPopupUiController(this);
        }
        
        private void ShowHideLastPopup(bool show)
        {
            if (_popupStack.Count == 0)
            {
                return;
            }

            var currentPopup = _popupStack.Peek();
            if (currentPopup != null)
            {
                currentPopup.SetActive(show);
            }
        }

        public void ShowPopup(IFaction faction)
        {
            var factionPopupWindow = Instantiate(_factionPopupWindowPrefab, _popupContainer);
            factionPopupWindow.SetupPopup(faction, this);

            ShowHideLastPopup(false);

            _popupStack.Push(factionPopupWindow.gameObject);

            _popupBackground.SetActive(true);
        }

        public void ClosePopup()
        {
            if (_popupStack.Count == 0)
            {
                return;
            }

            var popup = _popupStack.Pop();
            Destroy(popup);

            ShowHideLastPopup(true);

            _popupBackground.SetActive(_popupStack.Count > 0);
        }
    }
}