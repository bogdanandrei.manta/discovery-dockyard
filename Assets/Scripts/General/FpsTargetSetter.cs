using UnityEngine;

namespace General
{
    public class FpsTargetSetter : MonoBehaviour
    {
        private const int TARGET_FPS = 60;
        
        private void Awake()
        {
            Application.targetFrameRate = TARGET_FPS;
        }
    }
}