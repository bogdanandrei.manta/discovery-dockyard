using Cysharp.Threading.Tasks;
using DependencyInjection;
using DoTween;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace TabView
{
    public class View : InjectingMonoBehaviour
    {
        #if DOTWEEN
        [Title("Animations")]
        [DarkBox]
        [SerializeField]
        private bool _useAnimations;
        
        [ShowIf("_useAnimations")]
        [Required]
        [DarkBox]
        [SerializeField]
        private BaseDoTween _showAnimation;
        
        [ShowIf("_useAnimations")]
        [Required]
        [DarkBox]
        [SerializeField]
        private BaseDoTween _hideAnimation;
        #endif
        
        private void DisableOnHide()
        {
            gameObject.SetActive(false);
        }
        
        public virtual UniTask Show()
        {
#if DOTWEEN
            gameObject.SetActive(true);

            return _useAnimations ? _showAnimation.PlayAsTask() : UniTask.CompletedTask;
#else
            gameObject.SetActive(true);
            return UniTask.CompletedTask;
#endif
        }

        public virtual UniTask Hide()
        {
#if DOTWEEN
            if (_useAnimations)
            {
                _hideAnimation._onCompleteEvent.RemoveListener(DisableOnHide);
                _hideAnimation._onCompleteEvent.AddListener(DisableOnHide);
                return _hideAnimation.PlayAsTask();
            }
            
            gameObject.SetActive(false);
            
            return UniTask.CompletedTask;
#else
            gameObject.SetActive(false);
#endif
        }
    }
}