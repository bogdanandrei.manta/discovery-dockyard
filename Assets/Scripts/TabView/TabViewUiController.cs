using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using OdinInspector;
using UnityEngine;

namespace TabView
{
    public class TabViewUiController : MonoBehaviour
    {
        [DarkBox]
        [SerializeField]
        private TabViewItem[] _tabViewItems;

        [DarkBox]
        [SerializeField]
        private bool _showInitialViewOnStart = true;

        private TabViewItem _currentTabViewItem;

        private async void Start()
        {
            await HideAllViewsAsync();

            if (!_showInitialViewOnStart)
            {
                return;
            }

            ShowInitialView();
        }

        private void OnEnable()
        {
            foreach (var item in _tabViewItems)
            {
                item.TabButton.OnTabSelected += OnTabSelected;
            }
        }

        private void OnDisable()
        {
            foreach (var item in _tabViewItems)
            {
                item.TabButton.OnTabSelected -= OnTabSelected;
            }
        }

        private async void OnTabSelected(TabButton selectedTabButton)
        {
            if (_currentTabViewItem != null && _currentTabViewItem.TabButton == selectedTabButton)
            {
                return;
            }

            await UniTask.WhenAll(HideAllViewsAsync());

            var tabViewItem = _tabViewItems.FirstOrDefault(x => x.TabButton == selectedTabButton);
            tabViewItem?.View.Show();
            tabViewItem?.TabButton.ShowSelectedIndicator();

            _currentTabViewItem = tabViewItem;
        }

        private IEnumerable<UniTask> HideAllViewsAsync()
        {
            var tasks = new List<UniTask>();

            foreach (var tabViewItem in _tabViewItems)
            {
                tabViewItem.TabButton.HideSelectedIndicator();
                tasks.Add(tabViewItem.View.Hide());
            }

            return tasks;
        }

        private void OnValidate()
        {
            if (_tabViewItems == null || _tabViewItems.Length == 0)
            {
                return;
            }

            int initialActivated = _tabViewItems.Count(x => x.InitialActivated);

            switch (initialActivated)
            {
                case 0:
                    _tabViewItems[0].InitialActivated = true;
                    return;
                case <= 1:
                    return;
            }

            Debug.LogWarning("Only one tab can be activated on start. Keeping the first one. Deactivating the rest.");

            var first = _tabViewItems.First(x => x.InitialActivated);

            foreach (var item in _tabViewItems.Where(x => x != first))
            {
                item.InitialActivated = false;
            }
        }

        public async void HideAllViews()
        {
            await HideAllViewsAsync();
        }

        public void ShowInitialView()
        {
            var initialView = _tabViewItems.FirstOrDefault(x => x.InitialActivated);
            initialView?.View.Show();
            initialView?.TabButton.ShowSelectedIndicator();

            _currentTabViewItem = initialView;
        }
    }
}