using System;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace TabView
{
    [Serializable]
    public class TabViewItem
    {
        [field: SerializeField]
        [field: DarkBox]
        [field: Required]
        [field: ChildGameObjectsOnly]
        public TabButton TabButton { get; private set; }

        [field: SerializeField]
        [field: DarkBox]
        [field: Required]
        [field: ChildGameObjectsOnly]
        public View View { get; private set; }
        
        [field: SerializeField]
        public bool InitialActivated { get; set; }
    }
}