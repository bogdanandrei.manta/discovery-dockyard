using System;
using DG.Tweening;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace TabView
{
    public class TabButton : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Graphic _selectedIndicator;
        
        private Color _defaultColor;
        private Tweener _tweener;
        
        public event Action<TabButton> OnTabSelected; 
        
        private void Awake()
        {
            _defaultColor = _selectedIndicator.color;
        }
        
        public void TabSelected()
        {
            OnTabSelected?.Invoke(this);
        }
        
        public void ShowSelectedIndicator()
        {
            _tweener = _selectedIndicator.DOColor(Color.yellow, 1f).SetLoops(-1, LoopType.Yoyo);
        }
        
        public void HideSelectedIndicator()
        {
            _tweener.Kill();
            _selectedIndicator.color = _defaultColor;
        }
    }
}