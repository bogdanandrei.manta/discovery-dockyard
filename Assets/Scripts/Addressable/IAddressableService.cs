using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DependencyInjection;

namespace Addressable
{
    public interface IAddressableService : IService
    {
        UniTask<IList<T>> LoadAssets<T>(string address) where T : UnityEngine.Object;
        UniTask<T> LoadAsset<T>(string address) where T : UnityEngine.Object;
    }
}