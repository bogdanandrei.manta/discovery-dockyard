using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DependencyInjection;
using Logger;
using UnityEngine.AddressableAssets;

namespace Addressable
{
    public class AddressableService : IAddressableService
    {
        private const string ADDRESSABLE_CHANNEL = "Addressables";

        private ILoggerService _logger;
        
        public void Initialize(DependencyContainer container)
        {
            _logger = container.Resolve<ILoggerService>();
        }

        public async UniTask<IList<T>> LoadAssets<T>(string address) where T : UnityEngine.Object
        {
            // _logger.LogInfo($"Loading assets of type {nameof(T)} at {address}", ADDRESSABLE_CHANNEL);

            try
            {
                var handle = Addressables.LoadAssetsAsync<T>(address, null);
                await handle;

                return handle.Result;
            }
            catch (Exception e)
            {
                _logger.LogError($"Error while loading assets of type {nameof(T)} at {address}: {e.Message}", ADDRESSABLE_CHANNEL);
                throw;
            }
        }

        public async UniTask<T> LoadAsset<T>(string address) where T : UnityEngine.Object
        {
            try
            {
                var handle = Addressables.LoadAssetAsync<T>(address);
                await handle;

                return handle.Result;
            }
            catch (Exception e)
            {
                _logger.LogError($"Error while loading asset of type {nameof(T)} at {address}: {e.Message}", ADDRESSABLE_CHANNEL);
                throw;
            }
        }
    }
}