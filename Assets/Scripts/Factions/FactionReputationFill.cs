using System;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Factions
{
    public class FactionReputationFill : MonoBehaviour
    {
        [Required]
        [DarkBox]
        [SerializeField]
        private CanvasGroup _canvasGroup;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Image _fillImage;
        
        private bool _isInitialized;

        private void OnEnable()
        {
            _canvasGroup.alpha = _isInitialized ? 1 : 0;
        }

        public void SetFill(float fill)
        {
            _canvasGroup.alpha = fill > 0 ? 1 : 0;
            
            _fillImage.fillAmount = fill;
            
            _isInitialized = true;
        }
    }
}