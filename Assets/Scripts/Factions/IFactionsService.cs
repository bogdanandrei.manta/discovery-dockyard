using System.Collections.Generic;
using DependencyInjection;
using FreelancerDataTool.Interfaces;

namespace Factions
{
    public interface IFactionsService : IService
    {
        IEnumerable<IFaction> GetFactions();
        IFaction GetFactionByName(string name);
    }
}