using System;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using Unity.VisualScripting;
using UnityEngine;

namespace Factions
{
    public class FactionRumorsList : MonoBehaviour
    {
        [Required]
        [AssetList]
        [SerializeField]
        private FactionRumorsListItem _factionRumorsListItem;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _factionRumorsListContent;
        
        public void Setup(IFaction faction, Action<INpc> onRumorSelected)
        {
            if (faction.Npcs == null)
            {
                return;
            }

            foreach (var npc in faction.Npcs)
            {
                if (npc.Rumors is not { Count: > 0 })
                {
                    continue;
                }
                
                var factionRumorsListItem = Instantiate(_factionRumorsListItem, _factionRumorsListContent);
                factionRumorsListItem.Setup(npc, onRumorSelected);
            }
        }
    }
}