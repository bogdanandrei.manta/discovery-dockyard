using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Factions
{
    public class FactionShipsUsedListItem : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _shipName;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _shipType;
        
        private IShip _ship;
        
        public void Setup(IShip ship)
        {
            _ship = ship;
            _shipName.text = ship.Name?.Value;
            _shipType.text = ship.ShipClass.ToString();
        }
    }
}