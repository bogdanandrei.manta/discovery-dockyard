using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Factions
{
    public class FactionBasesOwnedList : MonoBehaviour
    {
        [Required]
        [AssetList]
        [SerializeField]
        private FactionBasesOwnedListItem _factionBasesOwnedListItem;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _factionShipsUsedListContent;
        
        public void Setup(IFaction faction)
        {
            if (faction.ShipsUsed == null)
            {
                return;
            }

            foreach (var @base in faction.OwnedBases)
            {
                var factionShipsUsedListItem = Instantiate(_factionBasesOwnedListItem, _factionShipsUsedListContent);
                factionShipsUsedListItem.Setup(@base);
            }
        }
    }
}