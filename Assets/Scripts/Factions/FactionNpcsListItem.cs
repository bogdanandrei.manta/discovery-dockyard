using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Factions
{
    public class FactionNpcsListItem : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcName;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcBase;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcSystem;
        
        private INpc _npc;
        
        public void Setup(INpc npc)
        {
            _npc = npc;
            _npcName.text = npc.Name?.Value?.Split("\n")[1];
            _npcBase.text = npc.Base?.Name?.Value;
            _npcSystem.text = npc.Base?.System?.Name?.Value;
        }
    }
}