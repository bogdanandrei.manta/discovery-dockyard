using System.Collections.Generic;
using DependencyInjection;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Popups;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Factions
{
    public class FactionReputationListItem : InjectingMonoBehaviour
    {
        [Title("Faction Name Text")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _factionNameText;
        
        [Title("Reputation Text")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _reputationText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _reputationValueText;
        
        [Title("Reputation Colors")]
        [Required]
        [DarkBox]
        [SerializeField]
        private Color _atWarColor;
        
        [Required]
        [DarkBox]
        [SerializeField]
        private Color _unfriendlyColor;
        
        [Required]
        [DarkBox]
        [SerializeField]
        private Color _neutralColor;
        
        [Required]
        [DarkBox]
        [SerializeField]
        private Color _friendlyColor;
        
        [Required]
        [DarkBox]
        [SerializeField]
        private Color _alliedColor;

        [Title("Reputation Bar")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private List<FactionReputationFill> _negativeFills;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private List<FactionReputationFill> _positiveFills;
        
        [Inject]
        private IPopupService _popupService;

        private const string AT_WAR = "At War";
        private const string UNFRIENDLY = "Unfriendly";
        private const string NEUTRAL = "Neutral";
        private const string FRIENDLY = "Friendly";
        private const string ALLIED = "Allied";
        
        private IFaction _faction;

        private void SetupReputationText(float reputation)
        {
            switch (reputation)
            {
                case -1f:
                    _reputationText.SetText(AT_WAR);
                    _reputationText.color = _atWarColor;
                    break;
                case > -1f and < -0.25f:
                    _reputationText.SetText(UNFRIENDLY);
                    _reputationText.color = _unfriendlyColor;
                    break;
                case >= -0.25f and < 0.25f:
                    _reputationText.SetText(NEUTRAL);
                    _reputationText.color = _neutralColor;
                    break;
                case >= 0.25f and < 1f:
                    _reputationText.SetText(FRIENDLY);
                    _reputationText.color = _friendlyColor;
                    break;
                case 1f:
                    _reputationText.SetText(ALLIED);
                    _reputationText.color = _alliedColor;
                    break;
            }
        }
        
        private void SetupReputationBar(float reputation)
        {
            var positiveIntReputation = Mathf.Abs((int)(reputation * 10));
            var extraFill = Mathf.Abs(reputation * 10) - positiveIntReputation;

            for (var i = 0; i < positiveIntReputation; i++)
            {
                var isLast = i == positiveIntReputation - 1;

                if (reputation < 0)
                {
                    _negativeFills[i].SetFill(1f);

                    if (isLast && extraFill > 0)
                    {
                        _negativeFills[i + 1].SetFill(extraFill);
                    }
                }
                else
                {
                    _positiveFills[i].SetFill(1f);

                    if (isLast && extraFill > 0)
                    {
                        _positiveFills[i + 1].SetFill(extraFill);
                    }
                }
            }
        }
        
        public void Setup(IFaction faction, float reputation)
        {
            _faction = faction;
            _factionNameText.SetText(faction.Name.Value);
            _reputationValueText.SetText(reputation.ToString("0.00"));

            SetupReputationText(reputation);
            SetupReputationBar(reputation);
        }
        
        public void OpenFactionWindow()
        {
            _popupService.ShowPopup(_faction);
        }
    }
}