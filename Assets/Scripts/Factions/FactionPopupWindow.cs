using DoTween;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Popups;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Factions
{
    public class FactionPopupWindow : MonoBehaviour, IPopupWindow<IFaction>
    {
        [Title("General")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _factionNameText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _factionShortNameText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _factionInternalNameText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Image _factionLogoImage;
        
        [Title("Infocard")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _factionInfoCardText;
        
        [Title("Reputation")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private FactionReputationList _factionReputationList;
        
        [Title("Ships Used")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private FactionShipsUsedList _factionShipsUsedList;
        
        [Title("Bases Owned")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private FactionBasesOwnedList _factionBasesOwnedList;
        
        [Title("Bribes")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private FactionBribesList _factionBribesList;
        
        [Title("Rumors")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private FactionRumorsList _factionRumorsList;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private FactionRumorPopup _factionRumorPopup;
        
        [Title("Npcs")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private FactionNpcsList _factionNpcsList;
        
        [Title("Extra Info")]
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _idsNameValueText;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _idsInfoValueText;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _idsShortNameValueText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _legalityValueText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _jumpPrefValueText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _scanAnnounceValueText;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _scanChanceValueText;
        
        [Title("Animation")]
        [Required]
        [SerializeField]
        private BaseDoTween _baseDoTween;

        private PopupUiController _popupUiController;
        
        private void OnRumorSelected(INpc npc)
        {
            _factionRumorPopup.ShowRumor(npc);
            _factionRumorPopup.gameObject.SetActive(true);
        }

        public void SetupPopup(IFaction faction, PopupUiController popupUiController)
        {
            _popupUiController = popupUiController;
            
            _factionNameText.SetText(faction.Name.Value);
            _factionShortNameText.SetText(faction.ShortName.Value);
            _factionInternalNameText.SetText(faction.Nickname);
            _factionLogoImage.sprite = faction.Logo;
            
            _factionInfoCardText.SetText(faction.Infocard.Value);
            
            _factionReputationList.Setup(faction);
            
            _factionShipsUsedList.Setup(faction);
            
            _factionBasesOwnedList.Setup(faction);
            
            _factionBribesList.Setup(faction);
            
            _factionRumorsList.Setup(faction, OnRumorSelected);
            
            _factionNpcsList.Setup(faction);
            
            _idsNameValueText.SetText(faction.Name.Key.ToString());
            _idsInfoValueText.SetText(faction.Infocard.Key.ToString());
            _idsShortNameValueText.SetText(faction.ShortName.Key.ToString());
            _legalityValueText.SetText(faction.Legality);
            _jumpPrefValueText.SetText(faction.JumpPreference);
            _scanAnnounceValueText.SetText(faction.ScanAnnounce.ToString());
            _scanChanceValueText.SetText(faction.ScanChance.ToString("P1"));
            
            _baseDoTween.PlayFrom();
        }

        public void ClosePopup()
        {
            _popupUiController.ClosePopup();
        }
    }
}