using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Factions
{
    public class FactionNpcsList : MonoBehaviour
    {
        [Required]
        [AssetList]
        [SerializeField]
        private FactionNpcsListItem _factionNpcsListItem;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _factionNpcsListContent;
        
        public void Setup(IFaction faction)
        {
            if (faction.Npcs == null)
            {
                return;
            }

            foreach (var npc in faction.Npcs)
            {
                var factionNpcsListItem = Instantiate(_factionNpcsListItem, _factionNpcsListContent);
                factionNpcsListItem.Setup(npc);
            }
        }
    }
}