using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Factions
{
    public class FactionBribesList : MonoBehaviour
    {
        [Required]
        [AssetList]
        [SerializeField]
        private FactionBribesListItem _factionBribesListItem;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _factionBribesListContent;
        
        public void Setup(IFaction faction)
        {
            if (faction.NpcBribers == null)
            {
                return;
            }

            foreach ((var npc, float cost) in faction.NpcBribers)
            {
                var factionBribesListItem = Instantiate(_factionBribesListItem, _factionBribesListContent);
                factionBribesListItem.Setup(npc, cost);
            }
        }
    }
}