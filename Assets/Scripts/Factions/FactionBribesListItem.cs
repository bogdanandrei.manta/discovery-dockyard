using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Factions
{
    public class FactionBribesListItem : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcName;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _bribeCost;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _bribeBaseName;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _bribeBaseSystem;
        
        private INpc _npc;

        public void Setup(INpc npc, float bribeCost)
        {
            _npc = npc;
            _npcName.text = npc.Name?.Value?.Split("\n")[1];
            _bribeCost.text = bribeCost.ToString("C0");
            _bribeBaseName.text = npc.Base?.Name?.Value;
            _bribeBaseSystem.text = npc.Base?.System?.Name?.Value;
        }
    }
}