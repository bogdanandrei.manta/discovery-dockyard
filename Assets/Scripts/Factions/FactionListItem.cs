using DependencyInjection;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Popups;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Factions
{
    public class FactionListItem : MonoBehaviour
    {
        [Title("Texts")]
        [Required]
        [DarkBox]
        [ChildGameObjectsOnly]
        [SerializeField]
        private TMP_Text _nameText;
        
        [Required]
        [DarkBox]
        [ChildGameObjectsOnly]
        [SerializeField]
        private TMP_Text _shortNameText;
        
        [Required]
        [DarkBox]
        [ChildGameObjectsOnly]
        [SerializeField]
        private TMP_Text _internalNameText;
        
        [Title("Images")]
        [Required]
        [DarkBox]
        [ChildGameObjectsOnly]
        [SerializeField]
        private Image _logoImage;
        
        private IFaction _faction;
        
        public void SetFaction(IFaction faction)
        {
            _nameText.text = faction.Name.Value;
            _shortNameText.text = faction.ShortName.Value;
            _internalNameText.text = faction.Nickname;

            _logoImage.sprite = faction.Logo;
            
            _faction = faction;
        }
        
        public void OnClick()
        {
            DependencyContainer.Instance.Resolve<IPopupService>().ShowPopup(_faction);
        }
    }
}