using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Factions
{
    public class FactionBasesOwnedListItem : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _baseName;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _baseSystem;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _baseSystemRegion;
        
        private IBase _base;
        public void Setup(IBase @base)
        {
            _base = @base;
            _baseName.text = @base.Name?.Value;
            _baseSystem.text = @base.System?.Name?.Value;
            _baseSystemRegion.text = @base.System?.Region;
        }
    }
}