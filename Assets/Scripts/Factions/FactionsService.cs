using System.Collections.Generic;
using Addressable;
using DependencyInjection;
using FreelancerDataTool.Interfaces;
using FreelancerDataTool.ScriptableObjects;
using Logger;

namespace Factions
{
    public class FactionsService : IFactionsService
    {
        private const string FACTIONS = "Factions";

        private readonly Dictionary<string, IFaction> _factionsByName = new();
        private readonly Dictionary<string, IFaction> _loadedFactionsByShortName = new();
        private readonly Dictionary<string, IFaction> _loadedFactionsByNickname = new();
        private readonly List<IFaction> _factions = new();

        private ILoggerService _logger;
        private IAddressableService _addressableService;

        public void Initialize(DependencyContainer container)
        {
            _logger = container.Resolve<ILoggerService>();
            _addressableService = container.Resolve<IAddressableService>();

            LoadFactions();
        }

        private async void LoadFactions()
        {
            var loadedFactions = await _addressableService.LoadAssets<FactionSO>("Factions");
            foreach (var faction in loadedFactions)
            {
                _factionsByName[faction.Name.Value] = faction;
                _loadedFactionsByShortName[faction.ShortName.Value] = faction;
                _loadedFactionsByNickname[faction.Nickname] = faction;
                
                _factions.Add(faction);
            }
        }

        public IEnumerable<IFaction> GetFactions()
        {
            return _factions;
        }

        public IFaction GetFactionByName(string name)
        {
            if (_factionsByName.TryGetValue(name, out var faction))
            {
                return faction;
            }

            if (_loadedFactionsByShortName.TryGetValue(name, out faction))
            {
                return faction;
            }

            if (_loadedFactionsByNickname.TryGetValue(name, out faction))
            {
                return faction;
            }

            _logger.LogError($"Faction with name {name} not found.", FACTIONS);
            return null;
        }
    }
}