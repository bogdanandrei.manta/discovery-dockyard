using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Factions
{
    public class FactionReputationList : MonoBehaviour
    {
        [Required]
        [AssetList]
        [SerializeField]
        private FactionReputationListItem _factionReputationListItemPrefab;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _factionReputationListContent;

        public void Setup(IFaction faction)
        {
            if (faction.Reputations == null)
            {
                return;
            }

            foreach ((var repFaction, float repValue) in faction.Reputations)
            {
                var factionReputationListItem = Instantiate(_factionReputationListItemPrefab, _factionReputationListContent);
                factionReputationListItem.Setup(repFaction, repValue);
            }
        }
    }
}