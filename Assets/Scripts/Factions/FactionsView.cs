using Cysharp.Threading.Tasks;
using DependencyInjection;
using OdinInspector;
using Sirenix.OdinInspector;
using TabView;
using UnityEngine;

namespace Factions
{
    public class FactionsView : View
    {
        [Required]
        [AssetList]
        [SerializeField]
        private FactionListItem _factionListItemPrefab;

        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _content;

        [Inject]
        private IFactionsService _factionsService;

        private bool _listInitialized;

        public override UniTask Show()
        {
            if (_listInitialized)
            {
                return base.Show();
            }

            var factions = _factionsService.GetFactions();
            foreach (var faction in factions)
            {
                var factionListItem = Instantiate(_factionListItemPrefab, _content);
                factionListItem.SetFaction(faction);
            }

            _listInitialized = true;

            return base.Show();
        }
    }
}