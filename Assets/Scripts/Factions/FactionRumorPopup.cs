using System.Text;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Factions
{
    public class FactionRumorPopup : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcName;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcBaseLocation;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _rumorText;
        
        private readonly StringBuilder _stringBuilder = new();

        public void ShowRumor(INpc npc)
        {
            _npcName.text = npc.Name?.Value?.Split("\n")[1];
            _npcBaseLocation.text = npc.Base.Name.Value;
            
            _stringBuilder.Clear();
            foreach (var rumor in npc.Rumors)
            {
                _stringBuilder.Append("\u2022<indent=1em>");
                _stringBuilder.Append(rumor.Value.TrimEnd());
                _stringBuilder.Append("</indent>\n");
            }
            
            _rumorText.text = _stringBuilder.ToString();
        }
    }
}