using System;
using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Factions
{
    public class FactionRumorsListItem : MonoBehaviour
    {
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcName;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private TMP_Text _npcBaseLocation;
        
        private INpc _npc;
        private Action<INpc> _onRumorSelected;

        public void Setup(INpc npc, Action<INpc> onRumorSelected)
        {
            _npc = npc;
            _npcName.text = npc.Name?.Value?.Split("\n")[1];
            _npcBaseLocation.text = npc.Base.Name.Value;
            
            _onRumorSelected = onRumorSelected;
        }
        
        public void OnClick()
        {
            _onRumorSelected?.Invoke(_npc);
        }
    }
}