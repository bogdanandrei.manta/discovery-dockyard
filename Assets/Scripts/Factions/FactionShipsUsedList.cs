using FreelancerDataTool.Interfaces;
using OdinInspector;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Factions
{
    public class FactionShipsUsedList : MonoBehaviour
    {
        [Required]
        [AssetList]
        [SerializeField]
        private FactionShipsUsedListItem _factionShipsUsedListItemPrefab;
        
        [Required]
        [ChildGameObjectsOnly]
        [DarkBox]
        [SerializeField]
        private Transform _factionShipsUsedListContent;
        
        public void Setup(IFaction faction)
        {
            if (faction.ShipsUsed == null)
            {
                return;
            }

            foreach (var ship in faction.ShipsUsed)
            {
                var factionShipsUsedListItem = Instantiate(_factionShipsUsedListItemPrefab, _factionShipsUsedListContent);
                factionShipsUsedListItem.Setup(ship);
            }
        }
    }
}