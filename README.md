# Welcome to Discovery Dockyard - All-in-One Freelancer Discovery Tool!

**[DISCOVERY DOCKYARD](https://bogdanandrei-manta.gitlab.io/discovery-dockyard/)** is your ultimate companion for navigating the vast universe of Discoverty Freelancer. Packed with features designed to streamline your exploration, trading, and ship customization, Discovery Dockyard is the must-have tool for every Freelancer pilot.

**Stay Up-to-Date**: With automatic version tracking and continuous updates, you'll always have access to the latest features and improvements, constantly working to enhance your Freelancer experience.

**Explore with Ease**: With our integrated wiki and navigation map (SOON™️), you'll never get lost in space again. Explore new sectors, discover hidden treasures, and uncover the secrets of the galaxy with confidence.

**Trade Like a Pro**: Our trading tool provides real-time market data and insights, empowering you to make informed decisions and maximize your profits. Whether you're buying low, selling high, or negotiating contracts, Discovery Dockyard has you covered. (SOON™️)

**Customize Your Ship**: Tweak and tune your ship to perfection with our intuitive loadout configurator. Experiment with different weapons, modules, and equipment to find the perfect setup for your playstyle. (SOON™️)

# Features:
## Wiki 
Access a comprehensive database of Freelancer knowledge to aid your exploration.
## NavMap
Navigate the galaxy with ease using our integrated map system.
## Trading Tool
 Utilize real-time market data to optimize your trading strategies.